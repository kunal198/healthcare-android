package com.natalie.healthcare;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by brst-pc20 on 1/25/17.
 */

public class HealthCareApplication extends Application {

    public static HealthCareApplication instance;

    @Override
    public void onCreate() {

        super.onCreate();
        instance=this;
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(this)

                .name("healthcare_realm")
                .schemaVersion(0)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);

    }

    public static synchronized HealthCareApplication getInstance()
    {
        return instance;
    }


}
