package com.natalie.healthcare.Adapters;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.TextView;

import com.natalie.healthcare.Bean.MedicationPlansBean;
import com.natalie.healthcare.R;

import java.util.ArrayList;

/**
 * Created by brst-pc20 on 3/28/17.
 */

public class MedicationChartAdpater extends RecyclerView.Adapter<MedicationChartAdpater.MyViewHolder> implements View.OnTouchListener, DatePickerDialog.OnDateSetListener {

    private ArrayList<MedicationPlansBean> charList;
    Context context;
    Activity activity;
    RecyclerView recyclerView;
    MyViewHolder myViewHolder;
    int pos;
    boolean disableViews=true;
    AutoCompleteTextView autoCompleteTextView1,autoCompleteTextView2,autoCompleteTextView3,autoCompleteTextView4;
    public String[] morning_arr = {"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",};
    public String[] lunch_arr = {"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",};
    public String[] tea_arr = {"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",};
    public String[] night_arr = {"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",};
    int width;

    public MedicationChartAdpater(Context context, Activity activity, RecyclerView recyclerView, ArrayList<MedicationPlansBean> charList, int width) {
        this.charList = charList;
        this.context = context;
        this.activity = activity;

        this.recyclerView = recyclerView;
        this.width = width;
    }


    @Override
    public MedicationChartAdpater.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.medication_chart_row, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        myViewHolder = holder;

        holder.noTV.setText("" + charList.get(position).getSerialNo());
        morning_arr[position] = charList.get(position).getMorning();
        lunch_arr[position] = charList.get(position).getLunch();
        tea_arr[position] = charList.get(position).getTea();
        night_arr[position] = charList.get(position).getNight();
        holder.morningTV.setText("" + morning_arr[position]);
        holder.lunchTV.setText("" + lunch_arr[position]);
        holder.teaTV.setText("" + tea_arr[position]);
        holder.nightTV.setText("" + night_arr[position]);
        holder.morningTV.setEnabled(disableViews);
        holder.lunchTV.setEnabled(disableViews);
        holder.teaTV.setEnabled(disableViews);
        holder.nightTV.setEnabled(disableViews);
        holder.morningTV.setOnTouchListener(this);

    }


    @Override
    public int getItemCount() {
        return charList.size();
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {

        if (view.getId() == R.id.morningTV) {
            view.getParent().requestDisallowInterceptTouchEvent(true);
            switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_UP:
                    recyclerView.requestDisallowInterceptTouchEvent(false);
                    break;
                case MotionEvent.ACTION_DOWN:
                    recyclerView.requestDisallowInterceptTouchEvent(false);
                    break;
            }
        }
        return false;
    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView noTV;
        public AutoCompleteTextView morningTV, lunchTV, teaTV, nightTV;

        //com.tbi.healthcare.AutoResizeTextView morningTV;//
        public MyViewHolder(View itemView) {
            super(itemView);
            Log.d("itemView", "==" + itemView);

            noTV = (TextView) itemView.findViewById(R.id.noTV);
            morningTV = (AutoCompleteTextView) itemView.findViewById(R.id.morningTV);
            lunchTV = (AutoCompleteTextView) itemView.findViewById(R.id.lunchTV);
            teaTV = (AutoCompleteTextView) itemView.findViewById(R.id.teaTV);
            nightTV = (AutoCompleteTextView) itemView.findViewById(R.id.nightTV);

            noTV.getLayoutParams().width = width;
            noTV.getLayoutParams().height = width;
            morningTV.getLayoutParams().width = width;
            lunchTV.getLayoutParams().width = width;
            teaTV.getLayoutParams().width = width;
            nightTV.getLayoutParams().width = width;


            morningTV.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {


                    Log.d("textwatcher", "onTextChanged" + s + "start" + start + "before" + before + "count" + count);
                    // TODO Auto-generated method stub
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    Log.d("textwatcher", "beforeTextChanged" + s + "start" + start + "count" + count);
                    // TODO Auto-generated method stub
                }

                @Override
                public void afterTextChanged(Editable s) {
                    morning_arr[getAdapterPosition()] = s.toString();
                    Log.d("textwatcher", "afterTextChanged" + s);
                    // TODO Auto-generated method stub
                }
            });
            lunchTV.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {


                    Log.d("textwatcher", "onTextChanged" + s + "start" + start + "before" + before + "count" + count);
                    // TODO Auto-generated method stub
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    Log.d("textwatcher", "beforeTextChanged" + s + "start" + start + "count" + count);
                    // TODO Auto-generated method stub
                }

                @Override
                public void afterTextChanged(Editable s) {
                    lunch_arr[getAdapterPosition()] = s.toString();
                    Log.d("textwatcher", "afterTextChanged" + s + pos);
                    // TODO Auto-generated method stub
                }
            });
            teaTV.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {


                    Log.d("textwatcher", "onTextChanged" + s + "start" + start + "before" + before + "count" + count);
                    // TODO Auto-generated method stub
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    Log.d("textwatcher", "beforeTextChanged" + s + "start" + start + "count" + count);
                    // TODO Auto-generated method stub
                }

                @Override
                public void afterTextChanged(Editable s) {
                    tea_arr[getAdapterPosition()] = s.toString();
                    Log.d("textwatcher", "afterTextChanged" + s);
                    // TODO Auto-generated method stub
                }
            });
            nightTV.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {


                    Log.d("textwatcher", "onTextChanged" + s + "start" + start + "before" + before + "count" + count);
                    // TODO Auto-generated method stub
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    Log.d("textwatcher", "beforeTextChanged" + s + "start" + start + "count" + count);
                    // TODO Auto-generated method stub
                }

                @Override
                public void afterTextChanged(Editable s) {
                    night_arr[getAdapterPosition()] = s.toString();
                    Log.d("textwatcher", "afterTextChanged" + s);
                    // TODO Auto-generated method stub
                }
            });
        }


    }

    public void resetView(boolean state) {
        this.disableViews=state;
    }
}
