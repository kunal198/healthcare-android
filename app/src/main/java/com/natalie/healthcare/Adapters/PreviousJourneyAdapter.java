package com.natalie.healthcare.Adapters;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.natalie.healthcare.Bean.RealmDataBeans;
import com.natalie.healthcare.R;

import io.realm.RealmResults;

/**
 * Created by brst-pc20 on 2/7/17.
 */

public class PreviousJourneyAdapter extends RecyclerView.Adapter<PreviousJourneyAdapter.MyViewHolder> {

    Context context;
    RealmResults<RealmDataBeans> results;
    public PreviousJourneyAdapter(Context context,  RealmResults<RealmDataBeans> results)
    {
        this.context=context;
        this.results=results;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.previous_journey_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {


        int pos_no=position+1;
        holder.indexTV.setText(""+pos_no);
        String source[]=results.get(position).getSourceData().split("@");
        String dest[]=results.get(position).getDestinationData().split("@");
        String checkin[]=results.get(position).getClockInTime().split("@");
        String checkout[]=results.get(position).getClockOutTime().split("@");
        String path[]=results.get(position).getMapImagePath().split("@");
        holder.sourceTV.setText(source[0]);
        holder.destinationTV.setText(dest[0]);
        holder.checkInTimeTV.setText(checkin[0]);
        holder.checkOutTimeTV.setText(checkout[0]);
        Log.d("log_tag","---"+path[0]);
        holder.mapScreenShotTV.setImageBitmap(BitmapFactory.decodeFile(path[0]));

    }

    @Override
    public int getItemCount() {
        Log.d("log_tag","---"+results.size());
        return results.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView indexTV,sourceTV,destinationTV,checkInTimeTV,checkOutTimeTV;
        private ImageView mapScreenShotTV;


        public MyViewHolder(View view) {
            super(view);
            indexTV= (TextView) view.findViewById(R.id.indexTV);
            sourceTV= (TextView) view.findViewById(R.id.sourceTV);
            destinationTV= (TextView) view.findViewById(R.id.destinationTV);
            mapScreenShotTV= (ImageView) view.findViewById(R.id.mapScreenShotTV);
            checkInTimeTV= (TextView) view.findViewById(R.id.checkInTimeTV);
            checkOutTimeTV= (TextView) view.findViewById(R.id.checkOutTimeTV);


        }

    }
}





