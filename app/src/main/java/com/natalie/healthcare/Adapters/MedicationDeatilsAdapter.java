package com.natalie.healthcare.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.natalie.healthcare.Bean.MedicationPlans;
import com.natalie.healthcare.Bean.ObjectWrapperForBinder;
import com.natalie.healthcare.Fragments.CarePlan;
import com.natalie.healthcare.Fragments.ClientSupportPlan;
import com.natalie.healthcare.Fragments.MedicationChart;
import com.natalie.healthcare.R;

import java.util.ArrayList;

/**
 * Created by brst-pc20 on 2/7/17.
 */

public class MedicationDeatilsAdapter extends RecyclerView.Adapter<MedicationDeatilsAdapter.MyViewHolder> {

    Context context;
    ArrayList<MedicationPlans> data_list;
    Fragment fragment;
    String client_id="",path="";

    RecyclerView recyclerView;
    public MedicationDeatilsAdapter(Context context, ArrayList<MedicationPlans> data_list, Fragment fragment,String client_id,RecyclerView recyclerView)
    {
        this.recyclerView=recyclerView;
        this.fragment=fragment;
        this.context=context;
        this.data_list=data_list;
        this.client_id=client_id;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, final int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.medication_plan_row, parent, false);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (path.equals("care_plans")) {
                    CarePlan carePlan = new CarePlan();

                    int index = recyclerView.indexOfChild(view);
                    Log.d("index", "---" + index);
                    MedicationPlans medicationPlans = data_list.get(index);
                    Bundle bundle = new Bundle();
                    bundle.putBinder("medical_plans", new ObjectWrapperForBinder(medicationPlans));
                    carePlan.setArguments(bundle);
                    fragment.getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragmentsContainer2, carePlan).addToBackStack(null)
                            .commit();
                } else if(path.equals("medical_plans")){
                    MedicationChart medicationChart = new MedicationChart();
                    int index = recyclerView.indexOfChild(view);
                    MedicationPlans medicationPlans = data_list.get(index);
                    Bundle bundle = new Bundle();
                    bundle.putBinder("medical_plans", new ObjectWrapperForBinder(medicationPlans));

                    medicationChart.setArguments(bundle);
                    fragment.getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragmentsContainer2, medicationChart).addToBackStack(null)
                            .commit();
                } else if(path.equals("client_support_plan")){
                    ClientSupportPlan clientSupportPlan = new ClientSupportPlan();
                    int index = recyclerView.indexOfChild(view);
                    MedicationPlans medicationPlans = data_list.get(index);
                    Bundle bundle = new Bundle();
                    bundle.putBinder("client_support_plan", new ObjectWrapperForBinder(medicationPlans));

                    clientSupportPlan.setArguments(bundle);
                    fragment.getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragmentsContainer2, clientSupportPlan).addToBackStack(null)
                            .commit();
                }
            }
        });

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        path=data_list.get(position).getPath();
        holder.dateTV.setText(data_list.get(position).getDate());
        final int pos_no=position+1;
        holder.noTV.setText(""+pos_no);


    }

    @Override
    public int getItemCount() {
        return data_list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView dateTV,noTV;


        public MyViewHolder(View view) {
            super(view);
            dateTV= (TextView) view.findViewById(R.id.dateTV);
            noTV= (TextView) view.findViewById(R.id.noTV);

        }

    }
}





