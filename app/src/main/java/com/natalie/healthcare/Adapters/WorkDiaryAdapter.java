package com.natalie.healthcare.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.natalie.healthcare.Bean.ClientsData;
import com.natalie.healthcare.R;

import java.util.ArrayList;

/**
 * Created by brst-pc20 on 2/7/17.
 */

public class WorkDiaryAdapter extends RecyclerView.Adapter<WorkDiaryAdapter.MyViewHolder> {

    Context context;
    ArrayList<ClientsData> data_list;

    public WorkDiaryAdapter(Context context, ArrayList<ClientsData> data_list)
    {
        this.context=context;
        this.data_list=data_list;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.work_diary_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.titleTV.setText(data_list.get(position).getClientName());
        holder.locationTV.setText(data_list.get(position).getClientLocation());
        holder.endHoursTV.setText("End Time: "+data_list.get(position).getEndTime());
        holder.startTimeTV.setText("Start Time: "+data_list.get(position).getStartTime());
        holder.workingHourTV.setText("Working Hours: "+data_list.get(position).getWorkingHour());

    }

    @Override
    public int getItemCount() {
        return data_list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView  locationTV, endHoursTV, startTimeTV,workingHourTV,titleTV;



        public MyViewHolder(View view) {
            super(view);
            titleTV= (TextView) view.findViewById(R.id.titleTV);
            locationTV= (TextView) view.findViewById(R.id.locationTV);
            endHoursTV= (TextView) view.findViewById(R.id.endHoursTV);
            startTimeTV= (TextView) view.findViewById(R.id.startTimeTV);
            workingHourTV= (TextView) view.findViewById(R.id.workingHourTV);
        }

    }
}
