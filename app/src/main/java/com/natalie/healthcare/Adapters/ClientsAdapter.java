package com.natalie.healthcare.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.natalie.healthcare.Bean.ClientsData;
import com.natalie.healthcare.Fragments.StartJourney;
import com.natalie.healthcare.R;

import java.util.ArrayList;

/**
 * Created by brst-pc20 on 2/7/17.
 */

public class ClientsAdapter extends RecyclerView.Adapter<ClientsAdapter.MyViewHolder> {

    Context context;
    ArrayList<ClientsData> data_list;
    Fragment fragment;
    public ClientsAdapter(Context context,ArrayList<ClientsData> data_list,Fragment fragment)
    {
        this.fragment=fragment;
        this.context=context;
        this.data_list=data_list;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.client_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.titleTV.setText(data_list.get(position).getClientName());
        final int pos_no=position+1;
        holder.noTV.setText(""+pos_no);
        holder.locationTV.setText(data_list.get(position).getClientLocation());
        holder.startNavigationTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bundle bundle=new Bundle();
                bundle.putString("lat",data_list.get(position).getClientLattitude());
                bundle.putString("lng",data_list.get(position).getClientLongitude());
                bundle.putString("address",data_list.get(position).getClientLocation());
                bundle.putString("client_name",data_list.get(position).getClientName());
                bundle.putString("client_id",data_list.get(position).getClientId());
                StartJourney startJourney=new StartJourney();
                startJourney.setArguments(bundle);
                fragment.getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragmentsContainer2, startJourney)

                        .commit();
            }
        });

    }

    @Override
    public int getItemCount() {
        return data_list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView titleTV,noTV,locationTV,startNavigationTV;


        public MyViewHolder(View view) {
            super(view);
            titleTV= (TextView) view.findViewById(R.id.titleTV);
            noTV= (TextView) view.findViewById(R.id.noTV);
            locationTV= (TextView) view.findViewById(R.id.locationTV);
            startNavigationTV= (TextView) view.findViewById(R.id.startNavigationTV);


        }

    }
}





