package com.natalie.healthcare.SharedPreferences;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by brst-pc93 on 11/24/16.
 */

public class MySharedPreferences {
    private static MySharedPreferences instance = null;

    public final String PreferenceName = "MyPreference";

    public static MySharedPreferences getInstance() {
        if (instance == null) {
            instance = new MySharedPreferences();
        }
        return instance;
    }


    public SharedPreferences getPreference(Context context) {

        return context.getSharedPreferences(PreferenceName, Activity.MODE_PRIVATE);
    }



    public void storeData(Context context,String key,String value) {

        SharedPreferences.Editor editor = getPreference(context).edit();
        editor.putString(key, value);
        editor.apply();


    }


    public void clearData(Context context,String key) {
        SharedPreferences preferences = context.getSharedPreferences(PreferenceName, Activity.MODE_PRIVATE);
        if (preferences != null)
            preferences.edit().remove(key).apply();

    }







    public String getData(Context context,String key) {

        return getPreference(context).getString(key, "null");
    }


    }

