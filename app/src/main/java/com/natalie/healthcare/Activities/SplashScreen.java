package com.natalie.healthcare.Activities;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.TextView;

import com.natalie.healthcare.MarshMallowPermissionClass.AbsRuntimeMarshmallowPermission;
import com.natalie.healthcare.R;
import com.natalie.healthcare.SharedPreferences.MySharedPreferences;

public class SplashScreen extends AbsRuntimeMarshmallowPermission {

    public static TextView logoutTV,fragActivitytitleTV;
    public ImageView addIV,backIV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        requestAppPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.INTERNET,Manifest.permission.CAMERA}, R.string.message, ALL_PERMISSIONS);


    }

    @Override
    public void onPermissionGranted(int requestCode) {
         if (requestCode == ALL_PERMISSIONS) {
             new Handler().postDelayed(new Runnable() {
                 @Override
                 public void run() {
                     boolean  session=MySharedPreferences.getInstance().getData(SplashScreen.this,"SESSION").equals("true");
                     if (session)
                     {
                         startActivity(new Intent(SplashScreen.this,HomePage.class));
                         overridePendingTransition(R.anim.animation_enter_from_right,R.anim.animation_leave_out_to_left);
                         finish();

                     }
                     else {
                         startActivity(new Intent(SplashScreen.this,MainActivity.class));
                         overridePendingTransition(R.anim.animation_enter_from_right,R.anim.animation_leave_out_to_left);
                         finish();
                     }
                 }
             },3000);
        }
    }


}
