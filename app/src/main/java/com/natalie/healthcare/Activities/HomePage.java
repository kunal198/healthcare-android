package com.natalie.healthcare.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.natalie.healthcare.Fragments.HomePageFragment;
import com.natalie.healthcare.R;
import com.natalie.healthcare.SharedPreferences.MySharedPreferences;
import com.natalie.healthcare.Utills.Constant;

public class HomePage extends AppCompatActivity implements View.OnClickListener {

    public static TextView logoutTV,fragActivitytitleTV;
    public ImageView addIV,backIV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_container_two);
        init();
        if (savedInstanceState == null) {
            HomePageFragment homePageFragment=new HomePageFragment();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragmentsContainer2, homePageFragment).addToBackStack(null)
                    .commit();
        }

    }


    private  void init()
    {
        Constant.USER_ID= MySharedPreferences.getInstance().getData(HomePage.this, "USER_ID");;

        logoutTV= (TextView) findViewById(R.id.logoutTV);
        backIV= (ImageView) findViewById(R.id.backIV);
        addIV= (ImageView) findViewById(R.id.addIV);
        fragActivitytitleTV= (TextView) findViewById(R.id.fragActivitytitleTV);
        backIV.setOnClickListener(this);
        logoutTV.setOnClickListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("ondestroyyy","==called==");
    }

    @Override
    public void onBackPressed() {


        int entry_count=getSupportFragmentManager().getBackStackEntryCount();
       Log.d("entry_count","--"+entry_count);
        if (entry_count<=1)
        {
            Constant.showAlert2(this,"Alert","Do you really want to log-out?");

        }
        else {
            super.onBackPressed();
           /* Fragment fragment = getSupportFragmentManager().findFragmentByTag("MYCLIENTS");
            if(fragment != null)
                getSupportFragmentManager().beginTransaction().remove(fragment).commit();*/
          //  StartJourney.clearAutocompleteView();
        }

        Log.d("entry",""+entry_count);

    }


    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.backIV:
                onBackPressed();
                break;
            case R.id.logoutTV:
                Constant.showAlert2(this,"Alert","Do you really want to log-out?");

                break;
        }
    }
}
