package com.natalie.healthcare.Activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.ImageView;

import com.natalie.healthcare.Fragments.ContactFragment;
import com.natalie.healthcare.Fragments.InformationFragment;
import com.natalie.healthcare.Fragments.LoginFragment;
import com.natalie.healthcare.Fragments.SignUpFragment;
import com.natalie.healthcare.R;

public class FragmentActivity extends AppCompatActivity implements View.OnClickListener{

    TextView fragActivitytitleTV,signInTV,informationTV,contactTV;
    ImageView backIV;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_container);
        String fragment = getIntent().getStringExtra("fragment");
        init();
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragmentsContainer, getFragmentReference(fragment)).addToBackStack(null)
                    .commit();
        }

    }


    private void init()
    {
        fragActivitytitleTV= (TextView) findViewById(R.id.fragActivitytitleTV);
        backIV= (ImageView) findViewById(R.id.backIV);
        backIV.setOnClickListener(this);
    }
    private Fragment getFragmentReference(String key) {
        Fragment fragment = null;

        switch (key) {

            case "SIGNIN":
                fragActivitytitleTV.setText("SIGN IN");
                fragment = new LoginFragment();

                break;
            case "REGISTERATION":
                fragActivitytitleTV.setText("REGISTRATION");
                fragment = new SignUpFragment();

                break;
            case "INFORMATION":
                fragActivitytitleTV.setText("INFORMATION");
                fragment = new InformationFragment();

                break;
            case "CONTACT":
                fragActivitytitleTV.setText("CONTACT");
                fragment = new ContactFragment();

                break;

        }

        return fragment;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        int entry_count=getSupportFragmentManager().getBackStackEntryCount();
        if (entry_count==0)
            finish();

    }


    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.backIV:
                onBackPressed();
                break;
        }
    }
}
