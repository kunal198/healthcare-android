package com.natalie.healthcare.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.natalie.healthcare.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextView registrationTV,signInTV,informationTV,contactTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.health_care_logo);

        init();
    }




    private void init()
    {
        registrationTV= (TextView) findViewById(R.id.registrationTV);
        signInTV= (TextView) findViewById(R.id.signInTV);
        informationTV= (TextView) findViewById(R.id.informationTV);
        contactTV= (TextView) findViewById(R.id.contactTV);
        registrationTV.setOnClickListener(this);
        signInTV.setOnClickListener(this);
        informationTV.setOnClickListener(this);
        contactTV.setOnClickListener(this);
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("ondestroyyy","==calledd==");
    }
    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.registrationTV:
                goToAnotherActivity("REGISTERATION");
                break;
            case R.id.signInTV:
                goToAnotherActivity("SIGNIN");
                break;
            case R.id.informationTV:
                goToAnotherActivity("INFORMATION");
                break;
            case R.id.contactTV:
                goToAnotherActivity("CONTACT");
                break;
        }

    }

    private void goToAnotherActivity(String frag_value)
    {
        Intent fragment_activityy = new Intent(this, FragmentActivity.class);

        fragment_activityy.putExtra("fragment", frag_value);

        startActivity(fragment_activityy);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        int entry_count=getSupportFragmentManager().getBackStackEntryCount();
        if (entry_count==0)
            finish();
    }
}
