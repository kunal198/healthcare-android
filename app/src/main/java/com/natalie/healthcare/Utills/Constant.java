package com.natalie.healthcare.Utills;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.natalie.healthcare.Activities.FragmentActivity;
import com.natalie.healthcare.Async_Thread.Super_AsyncTask;
import com.natalie.healthcare.Async_Thread.Super_AsyncTask_Interface;
import com.natalie.healthcare.CallBacks.GetClientsCallBack;
import com.natalie.healthcare.SharedPreferences.MySharedPreferences;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.realm.Realm;
import io.realm.RealmObject;


/**
 * Created by brst-pc20 on 1/3/17.
 */

public class Constant {

    public static String USER_ID = "";
     public static String SERVER_PATH = "http://homecare4eva.co.uk/backend/index.php/";
 //   public static String SERVER_PATH = "http://beta.homecare4eva.co.uk//backend/index.php/";
    public static String LOGIN = SERVER_PATH + "api/login";
    public static String SIGNUP = SERVER_PATH + "api/register";
    public static String CREATE_CLIENT = SERVER_PATH + "clients/newclient";
    public static String GET_CLIENTS = SERVER_PATH + "clients/get-clients";
    public static String GET_ASSIGNED_TASK = SERVER_PATH + "clients/get-assigned-tasks";
    public static String SAVE_JOURNEY = SERVER_PATH + "journeys/save-journey";
    public static String CLIENT_CARE_PLANS = SERVER_PATH + "api/client-care-plans";
    public static String MEDICATION_CARE_PLANS = SERVER_PATH + "api/medication-plans";
    public static String Client_support_plans = SERVER_PATH + "api/support-plans";

    public static String UPDATE_MEDICATION_CARE_PLANS = SERVER_PATH + "api/update-client-medication-plan";
    public static String SAVE_CLIENT_MEDICATION_CARE_PLANS = SERVER_PATH + "api/save-client-medication-plan";
    public static String SAVE_CARE_PLANS = SERVER_PATH + "api/save-client-care-plan";
    public static String UPDATE_CARE_PLANS = SERVER_PATH + "api/update-client-care-plan";




    public static ArrayList<String> category_list = new ArrayList<>();


    private static Context context;

    public Constant(Context context) {
        this.context = context;
    }


    public static void copyDataToRealm(Realm realm, RealmObject demoClass) {
        realm.beginTransaction();
        realm.copyToRealm(demoClass);
        realm.commitTransaction();
    }


    public static void hideKeyboard(Context ctx) {
        InputMethodManager inputManager = (InputMethodManager) ctx
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View v = ((Activity) ctx).getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }


    /* public static void initiatePopupWindow(String msg, Context con) {
         try {
 // We need to get the instance of the LayoutInflater
             LayoutInflater inflater = (LayoutInflater) con
                     .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
             View layout = inflater.inflate(R.layout.popup_screen, null, true);

             TextView alertMessageTV = (TextView) layout.findViewById(R.id.alertMessageTV);
             //  int height= (int) (screenHeight/2.5);
             final PopupWindow pwindo = new PopupWindow(layout, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
             pwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);

             if (!msg.isEmpty())
                 alertMessageTV.setText(msg);
             TextView okTV = (TextView) layout.findViewById(R.id.okTV);
             okTV.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     pwindo.dismiss();
                 }
             });
         } catch (Exception e) {
             e.printStackTrace();
         }
     }*/
    public static void execute(Super_AsyncTask asyncTask) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            asyncTask.execute();
        }

    }


    public static boolean validateFields(String data) {
        if (data.isEmpty()) {
            return false;
        }
        return true;
    }

    public static boolean validatelength(String data) {
        if (data.length() < 8) {
            return false;
        }
        return true;
    }

    public static boolean emailValidator(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static boolean matchPassword(String pass1, String pass2) {
        if (!pass1.equals(pass2)) {
            return true;
        }
        return false;
    }


    public static void showAlert(final Activity context, String title, String msg) {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete


                    }
                })
                /*.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })*/
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public static void showAlert2(final Activity context, String title, String msg) {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        MySharedPreferences.getInstance().storeData(context, "SESSION", "false");


                        Intent fragment_activityy = new Intent(context, FragmentActivity.class);

                        fragment_activityy.putExtra("fragment", "SIGNIN");

                        context.startActivity(fragment_activityy);
                        context.finish();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public static void getCLients(Context context, final GetClientsCallBack callBack) {
        String user_id = MySharedPreferences.getInstance().getData(context, "USER_ID");
        HashMap hashMap = new HashMap();
        hashMap.put("clients", "jatin");
        hashMap.put("cid", user_id);

        Constant.execute(new Super_AsyncTask(context, hashMap, Constant.GET_CLIENTS, new Super_AsyncTask_Interface() {

            @Override
            public void onTaskCompleted(String output) {

                callBack.onResult(output);

            }

        }, true, false));
    }
}
