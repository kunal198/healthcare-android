package com.natalie.healthcare;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.natalie.healthcare.Async_Thread.Super_AsyncTask;
import com.natalie.healthcare.Async_Thread.Super_AsyncTask_Interface;
import com.natalie.healthcare.CallBacks.CallBacks;
import com.natalie.healthcare.Utills.Constant;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class PlacesDetails {
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String TYPE_DETAIL = "/details";
    private static final String OUT_JSON = "/json";
    private static final String API_KEY ="AIzaSyAfC5H98Vrd9DO56NJLE_GeXIID3Iigg4Q";
    Context context;
    ArrayList resultList;
    CallBacks callBacks;
    String outputt="";

    //private static final String API_KEY = "------------ make your specific key ------------; // cle pour le serveur       
    public PlacesDetails(Context context) {

        this.context = context;

        // TODO Auto-generated constructor stub
    }

    public String getLatLng(String placeid, final CallBacks callBacks, final String origin) {


        Constant.execute(new Super_AsyncTask(context, PLACES_API_BASE + TYPE_DETAIL + OUT_JSON + "?placeid=" + placeid + "&key=" + API_KEY, new Super_AsyncTask_Interface() {

            @Override
            public void onTaskCompleted(String output) {
                Log.d("location_op", "" + output);
                if (origin.equals("SOURCE"))
                {

                    callBacks.latLngFromPlaceId(output,"SOURCE");
                }
                else {

                    callBacks.latLngFromPlaceId(output,"DESTINATION");
                }
              outputt = output;
            }

        }, true, false));


        return outputt;

    }


    public ArrayList<Double> placeDetail(final String input) {


        new AsyncTask<ArrayList<Double>, Void, ArrayList<Double>>() {


            @Override
            protected ArrayList<Double> doInBackground(ArrayList<Double>... arrayLists) {
                Log.d("log_tag", "input" + input);
                ArrayList<Double> resultList = null;

                HttpURLConnection conn = null;
                StringBuilder jsonResults = new StringBuilder();
                try {
                    StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_DETAIL + OUT_JSON);
                    sb.append("?placeid=" + URLEncoder.encode(input, "utf8"));
                    sb.append("&key=" + API_KEY);
                    URL url = new URL(sb.toString());
                    //Log.e("url", url.toString());
                    Log.d("URL: ", "--" + url);
                    System.out.println("******************************* connexion au serveur *****************************************");
                    //Log.e("nous sommes entrai de test la connexion au serveur", "test to connect to the api");
                    conn = (HttpURLConnection) url.openConnection();
                    InputStreamReader in = new InputStreamReader(conn.getInputStream());

                    // Load the results into a StringBuilder
                    int read;
                    char[] buff = new char[1024];
                    while ((read = in.read(buff)) != -1) {
                        jsonResults.append(buff, 0, read);

                    }
                    System.out.println("le json result" + jsonResults.toString());
                } catch (MalformedURLException e) {
                    Log.e("log_tag", "Error processing Places API URL", e);

                } catch (IOException e) {
                    Log.e("log_tag", "Error connecting to Places API", e);

                } finally {
                    if (conn != null) {
                        conn.disconnect();
                    }
                    System.out.println("******************************* fin de la connexion*************************************************");
                }
                Log.d("log_result", "--" + resultList);
                return resultList;
            }


            @Override
            protected void onPostExecute(ArrayList<Double> doubles) {
                super.onPostExecute(doubles);
                try {

                    // Create a JSON object hierarchy from the results
                    //Log.e("creation du fichier Json", "creation du fichier Json");
                    System.out.println("fabrication du Json Objet");
                    JSONObject jsonObj = new JSONObject(doubles.toString());
                    //JSONArray predsJsonArray = jsonObj.getJSONArray("html_attributions");
                    JSONObject result = jsonObj.getJSONObject("result").getJSONObject("geometry").getJSONObject("location");
                    System.out.println("la chaine Json " + result);
                    Double longitude = result.getDouble("lng");
                    Double latitude = result.getDouble("lat");
                    System.out.println("longitude et latitude " + longitude + latitude);
                    resultList = new ArrayList<Double>(result.length());
                    resultList.add(result.getDouble("lng"));
                    resultList.add(result.getDouble("lat"));
                    Log.d("log_result", "--" + resultList);

                } catch (JSONException e) {
                    Log.e("log_tag", "Cannot process JSON results", e);
                }


            }
        };

        return resultList;
    }

}