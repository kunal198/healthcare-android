package com.natalie.healthcare.Dialog;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.NumberPicker;

import com.natalie.healthcare.Fragments.MedicationChart;
import com.natalie.healthcare.R;
import com.natalie.healthcare.Utills.Constant;

import java.util.Calendar;

public class MonthYearPickerDialog extends DialogFragment {

  private static final int MAX_YEAR = 2099;
  private DatePickerDialog.OnDateSetListener listener;
  Context context;
  public MonthYearPickerDialog()
  {

  }
  public void setListener(DatePickerDialog.OnDateSetListener listener) {
    this.listener = listener;
  }

  @Override
  public Dialog onCreateDialog(Bundle savedInstanceState) {
    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
    // Get the layout inflater
    LayoutInflater inflater = getActivity().getLayoutInflater();
    Constant.hideKeyboard(getActivity());
    Calendar cal = Calendar.getInstance();

    View dialog = inflater.inflate(R.layout.picker, null);
    final NumberPicker monthPicker = (NumberPicker) dialog.findViewById(R.id.picker_month);
    final NumberPicker yearPicker = (NumberPicker) dialog.findViewById(R.id.picker_year);

    monthPicker.setMinValue(1);
    monthPicker.setMaxValue(12);
    monthPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
    yearPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
    monthPicker.setValue(cal.get(Calendar.MONTH) + 1);

    int year = cal.get(Calendar.YEAR);
    yearPicker.setMinValue(year);
    yearPicker.setMaxValue(MAX_YEAR);
    yearPicker.setValue(year);

    builder.setView(dialog)
        // Add action buttons
        .setPositiveButton("ok", new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int id) {
           // listener.onDateSet(null, yearPicker.getValue(), monthPicker.getValue(), 0);
              Log.d("yearrr","=="+yearPicker.getValue());
            MedicationChart.monthTV.setText(monthPicker.getValue()+"");
            MedicationChart.yearTV.setText(yearPicker.getValue()+"");
          }
        })
        .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int id) {
            MonthYearPickerDialog.this.getDialog().cancel();
          }
        });
    return builder.create();
  }
}