package com.natalie.healthcare.Dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.natalie.healthcare.Async_Thread.Super_AsyncTask;
import com.natalie.healthcare.Async_Thread.Super_AsyncTask_Interface;
import com.natalie.healthcare.Bean.RealmDataBeans;
import com.natalie.healthcare.Fragments.StartJourney;
import com.natalie.healthcare.R;
import com.natalie.healthcare.SharedPreferences.MySharedPreferences;
import com.natalie.healthcare.Utills.Constant;

import java.util.HashMap;

import io.realm.Realm;


/**
 * Created by brst-pc81 on 7/7/16.
 */
public class AddNote extends Dialog implements View.OnClickListener {

    EditText feedbackET;
    Fragment fragment;
    Context mContext;
    private Button doneBT;
    String source_address = "", destination_address = "", check_in_time = "", check_out_time = "", image_path = "", client_name = "";
    Realm realm;

    public AddNote(Fragment fragment, Context context, String client_name, String source_address, String destination_address, String check_in_time, String check_out_time, String image_path, Realm realm) {
        super(context);
        this.fragment = fragment;
        this.mContext = context;
        this.client_name = client_name;
        this.source_address = source_address;
        this.destination_address = destination_address;
        this.check_in_time = check_in_time;
        this.check_out_time = check_out_time;
        this.image_path = image_path;
        this.realm = realm;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setContentView(R.layout.add_note);
        doneBT = (Button) findViewById(R.id.doneBT);
        feedbackET = (EditText) findViewById(R.id.feedbackET);
        doneBT.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.doneBT:
                if (!feedbackET.getText().toString().isEmpty()) {
                    Constant.hideKeyboard(mContext);
                    String user_id = MySharedPreferences.getInstance().getData(mContext, "USER_ID");
                    String user_name = MySharedPreferences.getInstance().getData(mContext, "USER_NAME");
                    HashMap hashMap = new HashMap();
                    if (client_name.isEmpty()) {
                        client_name = "Client Not Selected";
                    }
                    hashMap.put("client_name", client_name);
                    hashMap.put("username", user_name);
                    hashMap.put("journey_start", source_address);
                    hashMap.put("clockin", check_in_time);
                    hashMap.put("clockout", check_out_time);
                    hashMap.put("feedback", feedbackET.getText().toString());
                    hashMap.put("journey_end", destination_address);
                    hashMap.put("clock_in_place", source_address);
                    hashMap.put("clock_out_place", source_address);
                    hashMap.put("uid", user_id);
                    Log.d("hashmap", "---" + hashMap.toString());
                    Constant.execute(new Super_AsyncTask(mContext, hashMap, Constant.SAVE_JOURNEY, new Super_AsyncTask_Interface() {

                        @Override
                        public void onTaskCompleted(String output) {
                            Log.d("save_journey", "--" + output);
                            StartJourney.isNeedToshowAddButton=false;
                            Constant.hideKeyboard(mContext);
                            RealmDataBeans realmDataBeans = new RealmDataBeans();
                            realmDataBeans.setId(1 + System.currentTimeMillis());
                            realmDataBeans.setSourceData(source_address + "@source");
                            realmDataBeans.setDestinationData(destination_address + "@destination");
                            realmDataBeans.setMapImagePath(image_path + "@path");
                            realmDataBeans.setClockInTime(check_in_time + "@checkin");
                            realmDataBeans.setClockOutTime(check_out_time + "@checkout");

                            Constant.copyDataToRealm(realm, realmDataBeans);

                            ((StartJourney) fragment).checkInTV.setText("Check In");
                            ((StartJourney) fragment).checkInTV.setVisibility(View.GONE);
                            dismiss();
                        }




                },true, false));

                }
                else{
                    Constant.showAlert(fragment.getActivity(),"Alert","Please type note.");
                }
                break;


            default:
                break;
        }

    }
}
