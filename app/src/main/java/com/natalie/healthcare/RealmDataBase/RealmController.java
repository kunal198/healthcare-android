package com.natalie.healthcare.RealmDataBase;


import android.app.Activity;
import android.app.Application;
import android.support.v4.app.Fragment;

import com.natalie.healthcare.Bean.RealmDataBeans;

import io.realm.Realm;
import io.realm.RealmResults;


public class RealmController {

    private static RealmController instance;
    private final Realm realm;

    public RealmController(Application application) {
        realm = Realm.getDefaultInstance();
    }

    public static RealmController with(Fragment fragment) {

        if (instance == null) {
            instance = new RealmController(fragment.getActivity().getApplication());
        }
        return instance;
    }

    public static RealmController with(Activity activity) {

        if (instance == null) {
            instance = new RealmController(activity.getApplication());
        }
        return instance;
    }

    public static RealmController with(Application application) {

        if (instance == null) {
            instance = new RealmController(application);
        }
        return instance;
    }

    public static RealmController getInstance() {

        return instance;
    }

    public Realm getRealm() {

        return realm;
    }

    //Refresh the realm istance
    public void refresh() {

        realm.refresh();
    }

    //clear all objects from RealmDataBeans.class
    public void clearAll() {
        realm.beginTransaction();
        realm.clear(RealmDataBeans.class);
        realm.commitTransaction();
    }

    //find all objects in the RealmDataBeans.class
    public RealmResults<RealmDataBeans> getRealmDataBeanss() {

        return realm.where(RealmDataBeans.class).findAll();
    }

    //query a single item with the given id
    public RealmDataBeans getRealmDataBeans(String id) {

        return realm.where(RealmDataBeans.class).equalTo("id", id).findFirst();
    }

    //check if RealmDataBeans.class is empty
    public boolean hasRealmDataBeanss() {

        return !realm.allObjects(RealmDataBeans.class).isEmpty();
    }

    //query example
    public RealmResults<RealmDataBeans> queryedRealmDataBeanss() {

        return realm.where(RealmDataBeans.class)
                .contains("author", "Author 0")
                .or()
                .contains("title", "Realm")
                .findAll();

    }
}
