package com.natalie.healthcare;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class PlaceAPI {

    private static final String TAG = PlaceAPI.class.getSimpleName();

    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";

   // private static final String API_KEY = "AIzaSyAnUj6z8afDjJZQMCW1bnVHzD1sEIBZMqc";
    private static final String API_KEY = "AIzaSyCXYhgTOdOMLK-n1VFABBMmdmq5o6YEVYQ";

    public ArrayList<String> autocomplete (final String input) {

        new AsyncTask<Void, Void, ArrayList<String>>() {
            @Override
            protected ArrayList doInBackground(Void... voids) {
                ArrayList<String> resultList = null;

                HttpURLConnection conn = null;
                StringBuilder jsonResults = new StringBuilder();

                try {
                    StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
                    sb.append("?location=30.70836332,76.69138937&radius=5000&strictbounds");
                    sb.append("&key=" + API_KEY);
                    sb.append("&types=(cities)");
                    sb.append("&input=" + URLEncoder.encode(input, "utf8"));

                    URL url = new URL(sb.toString());
                    conn = (HttpURLConnection) url.openConnection();
                    InputStreamReader in = new InputStreamReader(conn.getInputStream());

                    // Load the results into a StringBuilder
                    int read;
                    char[] buff = new char[1024];
                    while ((read = in.read(buff)) != -1) {
                        jsonResults.append(buff, 0, read);
                    }
                } catch (MalformedURLException e) {
                    Log.e(TAG, "Error processing Places API URL", e);

                } catch (IOException e) {
                    Log.e(TAG, "Error connecting to Places API", e);

                } finally {
                    if (conn != null) {
                        conn.disconnect();
                    }
                }

                try {
                    // Log.d(TAG, jsonResults.toString());
                    Log.e("json_results", "----"+jsonResults.toString() );
                    // Create a JSON object hierarchy from the results
                    JSONObject jsonObj = new JSONObject(jsonResults.toString());
                    JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

                    // Extract the Place descriptions from the results
                    resultList = new ArrayList<String>(predsJsonArray.length());
                    for (int i = 0; i < predsJsonArray.length(); i++) {
                        resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
                    }
                } catch (JSONException e) {
                    Log.e(TAG, "Cannot process JSON results", e);
                }
                return resultList;
            }

            @Override
            protected void onPostExecute(ArrayList<String> strings) {
                super.onPostExecute(strings);
                Log.d("result_of_places",""+strings);
                Log.d("result_of_places",""+strings.size());
            }
        }.execute();
       return null;
    }
}