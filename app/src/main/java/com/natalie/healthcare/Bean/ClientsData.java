package com.natalie.healthcare.Bean;

/**
 * Created by brst-pc20 on 2/23/17.
 */

public class ClientsData {

    String clientId,clientName,clientTelephoneNo,clientDoorCodes,clientLocation,clientLattitude,clientLongitude;

    String startTime,endTime,workingHour;

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getClientTelephoneNo() {
        return clientTelephoneNo;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public void setClientTelephoneNo(String clientTelephoneNo) {
        this.clientTelephoneNo = clientTelephoneNo;
    }

    public String getWorkingHour() {
        return workingHour;
    }

    public void setWorkingHour(String workingHour) {
        this.workingHour = workingHour;
    }

    public String getStartTime() {

        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getClientDoorCodes() {
        return clientDoorCodes;
    }

    public void setClientDoorCodes(String clientDoorCodes) {
        this.clientDoorCodes = clientDoorCodes;
    }

    public String getClientLocation() {
        return clientLocation;
    }

    public void setClientLocation(String clientLocation) {
        this.clientLocation = clientLocation;
    }

    public String getClientLattitude() {
        return clientLattitude;
    }

    public void setClientLattitude(String clientLattitude) {
        this.clientLattitude = clientLattitude;
    }

    public String getClientLongitude() {
        return clientLongitude;
    }

    public void setClientLongitude(String clientLongitude) {
        this.clientLongitude = clientLongitude;
    }
}
