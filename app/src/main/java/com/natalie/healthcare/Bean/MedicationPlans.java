package com.natalie.healthcare.Bean;

/**
 * Created by brst-pc20 on 3/8/17.
 */

public class MedicationPlans  {

    String id,user_id,client_id,date,start_time,end_time,communication,food,urine,bowles,print,signature,created_at,updated_at;

    String patientName,facility,dob,sex,medicationPharmacy,month,year,medicationTimings,diagnoseComment,allery,doctorName,doctorSignature,managerNmae,managerSignature;

    String path;

    String service_user_name,assessmentDate,reassessmentDate,lifeHistory,howToSupport,morningVisits,dinnerVisits,teaVisits,nightVisits,medicationSupport,supportAroundHome,socialNeedsEmotionalSupportRequirement,
    importantServiceUser,foodAlergies,otherAllergies,dietaryRequirements,favNonFavFoods,specialRequirements,supportHours,
    otherHours,healthHistory,dnar,sightHearing,mentalHealth,variousProblems,otherProblems,proneProblems,
    blisterPacks,eyedrops,medicationSafeNumber,medicineBottles,other,supportLevel,prescription,sideEffects,carePackageByHospital,personalInfo,gpDetails,
    socialWorkerName,socailWorkerTel,additionalInfo,frontDoorKeySafe,frontDoorKeySafeNumber,nominatedKeyHolderName,
    nominatedKeyHolderTel,lifeLine,stopClockLocation,gasPointLocation,electricityPointLocation,fuseBoxLocation,emergencyContacts,fallRisks,
    pressureScores,descisionSupportPlan,contigencyPlan,staffSelection,supportWorkerRequirements,
    additionalNotes,supportComment,frequencyDosage,medicationFrequency,informedConsent,pulse8ltd;


    public String getService_user_name() {
        return service_user_name;
    }

    public void setService_user_name(String service_user_name) {
        this.service_user_name = service_user_name;
    }

    public String getAssessmentDate() {
        return assessmentDate;
    }

    public void setAssessmentDate(String assessmentDate) {
        this.assessmentDate = assessmentDate;
    }

    public String getReassessmentDate() {
        return reassessmentDate;
    }

    public void setReassessmentDate(String reassessmentDate) {
        this.reassessmentDate = reassessmentDate;
    }

    public String getLifeHistory() {
        return lifeHistory;
    }

    public void setLifeHistory(String lifeHistory) {
        this.lifeHistory = lifeHistory;
    }

    public String getHowToSupport() {
        return howToSupport;
    }

    public void setHowToSupport(String howToSupport) {
        this.howToSupport = howToSupport;
    }

    public String getMorningVisits() {
        return morningVisits;
    }

    public void setMorningVisits(String morningVisits) {
        this.morningVisits = morningVisits;
    }

    public String getDinnerVisits() {
        return dinnerVisits;
    }

    public void setDinnerVisits(String dinnerVisits) {
        this.dinnerVisits = dinnerVisits;
    }

    public String getTeaVisits() {
        return teaVisits;
    }

    public void setTeaVisits(String teaVisits) {
        this.teaVisits = teaVisits;
    }

    public String getNightVisits() {
        return nightVisits;
    }

    public void setNightVisits(String nightVisits) {
        this.nightVisits = nightVisits;
    }

    public String getMedicationSupport() {
        return medicationSupport;
    }

    public void setMedicationSupport(String medicationSupport) {
        this.medicationSupport = medicationSupport;
    }

    public String getSupportAroundHome() {
        return supportAroundHome;
    }

    public void setSupportAroundHome(String supportAroundHome) {
        this.supportAroundHome = supportAroundHome;
    }

    public String getSocialNeedsEmotionalSupportRequirement() {
        return socialNeedsEmotionalSupportRequirement;
    }

    public void setSocialNeedsEmotionalSupportRequirement(String socialNeedsEmotionalSupportRequirement) {
        this.socialNeedsEmotionalSupportRequirement = socialNeedsEmotionalSupportRequirement;
    }

    public String getImportantServiceUser() {
        return importantServiceUser;
    }

    public void setImportantServiceUser(String importantServiceUser) {
        this.importantServiceUser = importantServiceUser;
    }

    public String getFoodAlergies() {
        return foodAlergies;
    }

    public void setFoodAlergies(String foodAlergies) {
        this.foodAlergies = foodAlergies;
    }

    public String getOtherAllergies() {
        return otherAllergies;
    }

    public void setOtherAllergies(String otherAllergies) {
        this.otherAllergies = otherAllergies;
    }

    public String getDietaryRequirements() {
        return dietaryRequirements;
    }

    public void setDietaryRequirements(String dietaryRequirements) {
        this.dietaryRequirements = dietaryRequirements;
    }

    public String getFavNonFavFoods() {
        return favNonFavFoods;
    }

    public void setFavNonFavFoods(String favNonFavFoods) {
        this.favNonFavFoods = favNonFavFoods;
    }

    public String getSpecialRequirements() {
        return specialRequirements;
    }

    public void setSpecialRequirements(String specialRequirements) {
        this.specialRequirements = specialRequirements;
    }

    public String getSupportHours() {
        return supportHours;
    }

    public void setSupportHours(String supportHours) {
        this.supportHours = supportHours;
    }

    public String getOtherHours() {
        return otherHours;
    }

    public void setOtherHours(String otherHours) {
        this.otherHours = otherHours;
    }

    public String getHealthHistory() {
        return healthHistory;
    }

    public void setHealthHistory(String healthHistory) {
        this.healthHistory = healthHistory;
    }

    public String getDnar() {
        return dnar;
    }

    public void setDnar(String dnar) {
        this.dnar = dnar;
    }

    public String getSightHearing() {
        return sightHearing;
    }

    public void setSightHearing(String sightHearing) {
        this.sightHearing = sightHearing;
    }

    public String getMentalHealth() {
        return mentalHealth;
    }

    public void setMentalHealth(String mentalHealth) {
        this.mentalHealth = mentalHealth;
    }

    public String getVariousProblems() {
        return variousProblems;
    }

    public void setVariousProblems(String variousProblems) {
        this.variousProblems = variousProblems;
    }

    public String getOtherProblems() {
        return otherProblems;
    }

    public void setOtherProblems(String otherProblems) {
        this.otherProblems = otherProblems;
    }

    public String getProneProblems() {
        return proneProblems;
    }

    public void setProneProblems(String proneProblems) {
        this.proneProblems = proneProblems;
    }

    public String getBlisterPacks() {
        return blisterPacks;
    }

    public void setBlisterPacks(String blisterPacks) {
        this.blisterPacks = blisterPacks;
    }

    public String getEyedrops() {
        return eyedrops;
    }

    public void setEyedrops(String eyedrops) {
        this.eyedrops = eyedrops;
    }

    public String getMedicationSafeNumber() {
        return medicationSafeNumber;
    }

    public void setMedicationSafeNumber(String medicationSafeNumber) {
        this.medicationSafeNumber = medicationSafeNumber;
    }

    public String getMedicineBottles() {
        return medicineBottles;
    }

    public void setMedicineBottles(String medicineBottles) {
        this.medicineBottles = medicineBottles;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public String getSupportLevel() {
        return supportLevel;
    }

    public void setSupportLevel(String supportLevel) {
        this.supportLevel = supportLevel;
    }

    public String getPrescription() {
        return prescription;
    }

    public void setPrescription(String prescription) {
        this.prescription = prescription;
    }

    public String getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(String sideEffects) {
        this.sideEffects = sideEffects;
    }

    public String getCarePackageByHospital() {
        return carePackageByHospital;
    }

    public void setCarePackageByHospital(String carePackageByHospital) {
        this.carePackageByHospital = carePackageByHospital;
    }

    public String getPersonalInfo() {
        return personalInfo;
    }

    public void setPersonalInfo(String personalInfo) {
        this.personalInfo = personalInfo;
    }

    public String getGpDetails() {
        return gpDetails;
    }

    public void setGpDetails(String gpDetails) {
        this.gpDetails = gpDetails;
    }

    public String getSocialWorkerName() {
        return socialWorkerName;
    }

    public void setSocialWorkerName(String socialWorkerName) {
        this.socialWorkerName = socialWorkerName;
    }

    public String getSocailWorkerTel() {
        return socailWorkerTel;
    }

    public void setSocailWorkerTel(String socailWorkerTel) {
        this.socailWorkerTel = socailWorkerTel;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public String getFrontDoorKeySafe() {
        return frontDoorKeySafe;
    }

    public void setFrontDoorKeySafe(String frontDoorKeySafe) {
        this.frontDoorKeySafe = frontDoorKeySafe;
    }

    public String getFrontDoorKeySafeNumber() {
        return frontDoorKeySafeNumber;
    }

    public void setFrontDoorKeySafeNumber(String frontDoorKeySafeNumber) {
        this.frontDoorKeySafeNumber = frontDoorKeySafeNumber;
    }

    public String getNominatedKeyHolderName() {
        return nominatedKeyHolderName;
    }

    public void setNominatedKeyHolderName(String nominatedKeyHolderName) {
        this.nominatedKeyHolderName = nominatedKeyHolderName;
    }

    public String getNominatedKeyHolderTel() {
        return nominatedKeyHolderTel;
    }

    public void setNominatedKeyHolderTel(String nominatedKeyHolderTel) {
        this.nominatedKeyHolderTel = nominatedKeyHolderTel;
    }

    public String getLifeLine() {
        return lifeLine;
    }

    public void setLifeLine(String lifeLine) {
        this.lifeLine = lifeLine;
    }

    public String getStopClockLocation() {
        return stopClockLocation;
    }

    public void setStopClockLocation(String stopClockLocation) {
        this.stopClockLocation = stopClockLocation;
    }

    public String getGasPointLocation() {
        return gasPointLocation;
    }

    public void setGasPointLocation(String gasPointLocation) {
        this.gasPointLocation = gasPointLocation;
    }

    public String getElectricityPointLocation() {
        return electricityPointLocation;
    }

    public void setElectricityPointLocation(String electricityPointLocation) {
        this.electricityPointLocation = electricityPointLocation;
    }

    public String getFuseBoxLocation() {
        return fuseBoxLocation;
    }

    public void setFuseBoxLocation(String fuseBoxLocation) {
        this.fuseBoxLocation = fuseBoxLocation;
    }

    public String getEmergencyContacts() {
        return emergencyContacts;
    }

    public void setEmergencyContacts(String emergencyContacts) {
        this.emergencyContacts = emergencyContacts;
    }

    public String getFallRisks() {
        return fallRisks;
    }

    public void setFallRisks(String fallRisks) {
        this.fallRisks = fallRisks;
    }

    public String getPressureScores() {
        return pressureScores;
    }

    public void setPressureScores(String pressureScores) {
        this.pressureScores = pressureScores;
    }

    public String getDescisionSupportPlan() {
        return descisionSupportPlan;
    }

    public void setDescisionSupportPlan(String descisionSupportPlan) {
        this.descisionSupportPlan = descisionSupportPlan;
    }

    public String getContigencyPlan() {
        return contigencyPlan;
    }

    public void setContigencyPlan(String contigencyPlan) {
        this.contigencyPlan = contigencyPlan;
    }

    public String getStaffSelection() {
        return staffSelection;
    }

    public void setStaffSelection(String staffSelection) {
        this.staffSelection = staffSelection;
    }

    public String getSupportWorkerRequirements() {
        return supportWorkerRequirements;
    }

    public void setSupportWorkerRequirements(String supportWorkerRequirements) {
        this.supportWorkerRequirements = supportWorkerRequirements;
    }

    public String getAdditionalNotes() {
        return additionalNotes;
    }

    public void setAdditionalNotes(String additionalNotes) {
        this.additionalNotes = additionalNotes;
    }

    public String getSupportComment() {
        return supportComment;
    }

    public void setSupportComment(String supportComment) {
        this.supportComment = supportComment;
    }

    public String getFrequencyDosage() {
        return frequencyDosage;
    }

    public void setFrequencyDosage(String frequencyDosage) {
        this.frequencyDosage = frequencyDosage;
    }

    public String getMedicationFrequency() {
        return medicationFrequency;
    }

    public void setMedicationFrequency(String medicationFrequency) {
        this.medicationFrequency = medicationFrequency;
    }

    public String getInformedConsent() {
        return informedConsent;
    }

    public void setInformedConsent(String informedConsent) {
        this.informedConsent = informedConsent;
    }

    public String getPulse8ltd() {
        return pulse8ltd;
    }

    public void setPulse8ltd(String pulse8ltd) {
        this.pulse8ltd = pulse8ltd;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getManagerNmae() {
        return managerNmae;
    }

    public void setManagerNmae(String managerNmae) {
        this.managerNmae = managerNmae;
    }

    public String getManagerSignature() {
        return managerSignature;
    }

    public void setManagerSignature(String managerSignature) {
        this.managerSignature = managerSignature;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getFacility() {
        return facility;
    }

    public void setFacility(String facility) {
        this.facility = facility;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getMedicationPharmacy() {
        return medicationPharmacy;
    }

    public void setMedicationPharmacy(String medicationPharmacy) {
        this.medicationPharmacy = medicationPharmacy;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMedicationTimings() {
        return medicationTimings;
    }

    public void setMedicationTimings(String medicationTimings) {
        this.medicationTimings = medicationTimings;
    }

    public String getDiagnoseComment() {
        return diagnoseComment;
    }

    public void setDiagnoseComment(String diagnoseComment) {
        this.diagnoseComment = diagnoseComment;
    }

    public String getAllery() {
        return allery;
    }

    public void setAllery(String allery) {
        this.allery = allery;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getDoctorSignature() {
        return doctorSignature;
    }

    public void setDoctorSignature(String doctorSignature) {
        this.doctorSignature = doctorSignature;
    }

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getFood() {
        return food;
    }

    public void setFood(String food) {
        this.food = food;
    }

    public String getCommunication() {
        return communication;
    }

    public void setCommunication(String communication) {
        this.communication = communication;
    }

    public String getUrine() {
        return urine;
    }

    public void setUrine(String urine) {
        this.urine = urine;
    }

    public String getBowles() {
        return bowles;
    }

    public void setBowles(String bowles) {
        this.bowles = bowles;
    }

    public String getPrint() {
        return print;
    }

    public void setPrint(String print) {
        this.print = print;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
