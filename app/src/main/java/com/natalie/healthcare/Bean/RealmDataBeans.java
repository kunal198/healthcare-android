package com.natalie.healthcare.Bean;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by brst-pc20 on 1/25/17.
 */

public class RealmDataBeans extends RealmObject {



    private String sourceData,destinationData,mapImagePath,clockInTime,clockOutTime;


    @PrimaryKey
    private long id;


    public String getSourceData() {
        return sourceData;
    }

    public void setSourceData(String sourceData) {
        this.sourceData = sourceData;
    }

    public String getDestinationData() {
        return destinationData;
    }

    public void setDestinationData(String destinationData) {
        this.destinationData = destinationData;
    }

    public String getClockInTime() {
        return clockInTime;
    }

    public void setClockInTime(String clockInTime) {
        this.clockInTime = clockInTime;
    }

    public String getMapImagePath() {
        return mapImagePath;
    }

    public void setMapImagePath(String mapImagePath) {
        this.mapImagePath = mapImagePath;
    }

    public String getClockOutTime() {
        return clockOutTime;
    }

    public void setClockOutTime(String clockOutTime) {
        this.clockOutTime = clockOutTime;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
