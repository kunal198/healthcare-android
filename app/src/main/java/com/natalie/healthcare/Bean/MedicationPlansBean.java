package com.natalie.healthcare.Bean;

/**
 * Created by brst-pc20 on 3/29/17.
 */

public class MedicationPlansBean {

    String serialNo,morning,lunch,tea,night;

    public String getMorning() {
        return morning;
    }

    public void setMorning(String morning) {
        this.morning = morning;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getLunch() {
        return lunch;
    }

    public void setLunch(String lunch) {
        this.lunch = lunch;
    }

    public String getTea() {
        return tea;
    }

    public void setTea(String tea) {
        this.tea = tea;
    }

    public String getNight() {
        return night;
    }



    public void setNight(String night) {
        this.night = night;
    }
}
