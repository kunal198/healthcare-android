package com.natalie.healthcare.Bean;

import android.os.Binder;

public class ObjectWrapperForBinder extends Binder {

    private final MedicationPlans mData;

    public ObjectWrapperForBinder(MedicationPlans data) {
        mData = data;
    }

    public MedicationPlans getData() {
        return mData;
    }
}