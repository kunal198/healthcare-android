package com.natalie.healthcare.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.natalie.healthcare.Activities.HomePage;
import com.natalie.healthcare.Async_Thread.Super_AsyncTask;
import com.natalie.healthcare.Async_Thread.Super_AsyncTask_Interface;
import com.natalie.healthcare.R;
import com.natalie.healthcare.SharedPreferences.MySharedPreferences;
import com.natalie.healthcare.Utills.Constant;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class SignUpFragment extends Fragment implements View.OnClickListener {

    private TextView registerContinueTV;
    private EditText nameTV, emailTV, passTV, confirmPassTV, managerIdTV;

    public SignUpFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Constant.hideKeyboard(getActivity());
        View view = inflater.inflate(R.layout.registration, container, false);
        initView(view);


        return view;
        // Inflate the layout for this fragment

    }

    private void initView(View view) {

        nameTV = (EditText) view.findViewById(R.id.nameTV);
        emailTV = (EditText) view.findViewById(R.id.emailTV);
        passTV = (EditText) view.findViewById(R.id.passTV);
        confirmPassTV = (EditText) view.findViewById(R.id.confirmPassTV);
        managerIdTV = (EditText) view.findViewById(R.id.managerIdTV);

        registerContinueTV = (TextView) view.findViewById(R.id.registerContinueTV);
        registerContinueTV.setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.registerContinueTV:
                if (validationOfData()) {
                    HashMap<String, String> hashMap = new HashMap();
                    hashMap.put("username", nameTV.getText().toString());
                    hashMap.put("password", passTV.getText().toString());
                    hashMap.put("email", emailTV.getText().toString());
                    hashMap.put("device_type","Android");
                    hashMap.put("unique_key", managerIdTV.getText().toString());
                    Constant.execute(new Super_AsyncTask(getActivity(), hashMap, Constant.SIGNUP, new Super_AsyncTask_Interface() {

                        @Override
                        public void onTaskCompleted(String output) {
                            Log.d("register_op", "" + output);
                            try {
                                JSONObject jsonObject = new JSONObject(output);

                                if (jsonObject.optString("success").equals("true")) {
                                    clearFields();
                                    MySharedPreferences.getInstance().storeData(getActivity(),"USER_ID",jsonObject.optString("id"));
                                    MySharedPreferences.getInstance().storeData(getActivity(),"SESSION","true");
                                    Intent fragment_activityy = new Intent(getActivity(), HomePage.class);
                                    startActivity(fragment_activityy);
                                }
                                else {
                                    Constant.showAlert(getActivity(),"Alert", jsonObject.optString("failure"));
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                    }, true, false));

                    break;
                }
        }

    }


    private void clearFields()
    {
        nameTV.setText("");
        passTV.setText("");
        confirmPassTV.setText("");
        emailTV.setText("");
        managerIdTV.setText("");
    }


    private boolean validationOfData() {
        boolean status=true;
        if (!Constant.validateFields(nameTV.getText().toString())) {
            nameTV.setError("Please Enter Your Name.");
            status=false;
        }

        if (!Constant.emailValidator(emailTV.getText().toString())) {
            emailTV.setError("Please Enter Valid Email Id.");
            status=false;
        }
        if (Constant.matchPassword(passTV.getText().toString(), confirmPassTV.getText().toString())) {
            confirmPassTV.setError("Confirm Password Not Matched.");
            status=false;
        }
        if (!Constant.validatelength(passTV.getText().toString())) {
            passTV.setError("Please Enter minimum 8 Digit password.");
            status=false;
        }   if (!Constant.validateFields(managerIdTV.getText().toString())) {
            managerIdTV.setError("Please Enter Manager Unique ID.");
            status=false;
        }
        return status;
    }

}
