package com.natalie.healthcare.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.natalie.healthcare.Activities.HomePage;
import com.natalie.healthcare.R;
import com.natalie.healthcare.Utills.Constant;

/**
 * Created by brst-pc20 on 1/6/17.
 */

public class HomePageFragment extends Fragment implements View.OnClickListener {

    TextView myClientsTV, startJourneyTV, previousJourneyTV, workDiaryTV;

    public HomePageFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Constant.hideKeyboard(getActivity());
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.home, container, false);
        init(view);


        return view;
    }

    private void init(View view) {

        myClientsTV = (TextView) view.findViewById(R.id.myClientsTV);
        startJourneyTV = (TextView) view.findViewById(R.id.startJourneyTV);
        previousJourneyTV = (TextView) view.findViewById(R.id.previousJourneyTV);
        workDiaryTV = (TextView) view.findViewById(R.id.workDiaryTV);
        myClientsTV.setOnClickListener(this);
        startJourneyTV.setOnClickListener(this);
        previousJourneyTV.setOnClickListener(this);
        workDiaryTV.setOnClickListener(this);
        ((HomePage)getActivity()).logoutTV.setVisibility(View.VISIBLE);
        ((HomePage)getActivity()).addIV.setVisibility(View.GONE);
        ((HomePage)getActivity()).backIV.setVisibility(View.GONE);
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.myClientsTV:
                goToFragment("MYCLIENTS");

                break;
            case R.id.startJourneyTV:
                goToFragment("MYJOURNEY");

                break;
            case R.id.previousJourneyTV:

                goToFragment("PreviousJOURNEY");
                break;
            case R.id.workDiaryTV:

                goToFragment("WORKDIARY");
                break;

        }

    }


    @Override
    public void onResume() {
        super.onResume();
        HomePage.fragActivitytitleTV.setText("Home");
        ((HomePage) getActivity()).logoutTV.setText("Logout");
        ((HomePage) getActivity()).addIV.setVisibility(View.GONE);
        if (StartJourney.sourceTV!=null)
        {
            StartJourney.sourceTV.setText("");
            StartJourney.destinationTV.setText("");
            StartJourney.isNeedToshowAddButton=false;
        }


    }

    private void goToFragment(String fragment)
    {

            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragmentsContainer2, getFragmentReference(fragment)).addToBackStack(null)
                    .commit();


    }
    private Fragment getFragmentReference(String key) {
        Fragment fragment = null;

        switch (key) {

            case "MYCLIENTS":

                fragment = new MyClients();

                break;
            case "MYJOURNEY":
                //fragActivitytitleTV.setText("REGISTERATION");
                fragment = new StartJourney();

                break;
            case "PreviousJOURNEY":
                //fragActivitytitleTV.setText("REGISTERATION");
                fragment = new PreviousJourney();

                break;
            case "WORKDIARY":
                //fragActivitytitleTV.setText("REGISTERATION");
                fragment = new WorkDiary();

                break;

        }

        return fragment;
    }
}
