package com.natalie.healthcare.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.natalie.healthcare.Activities.HomePage;
import com.natalie.healthcare.Async_Thread.Super_AsyncTask;
import com.natalie.healthcare.Async_Thread.Super_AsyncTask_Interface;
import com.natalie.healthcare.R;
import com.natalie.healthcare.SharedPreferences.MySharedPreferences;
import com.natalie.healthcare.Utills.Constant;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by brst-pc20 on 1/6/17.
 */

public class LoginFragment extends Fragment implements View.OnClickListener {

    TextView continueTV, termsConditionsTV, loginTV, userNameET, passwordET;

    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Constant.hideKeyboard(getActivity());
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.sign_in, container, false);
        initView(view);


        return view;
    }

    private void initView(View view)
    {
        passwordET = (TextView) view.findViewById(R.id.passwordET);
        userNameET = (TextView) view.findViewById(R.id.emailET);
       // loginTV = (TextView) view.findViewById(R.id.loginTV);
      //  forgotPassTV = (TextView) view.findViewById(R.id.forgotPassTV);
      //  signUpTV = (TextView) view.findViewById(R.id.signUpTV);
     //   signUpTV.setOnClickListener(this);
      //  forgotPassTV.setOnClickListener(this);
     //   loginTV.setOnClickListener(this);
        continueTV= (TextView) view.findViewById(R.id.continueTV);
        termsConditionsTV= (TextView) view.findViewById(R.id.termsConditionsTV);
        continueTV.setOnClickListener(this);
        termsConditionsTV.setOnClickListener(this);
    }

    private void clearFields()
    {
        userNameET.setText("");
        passwordET.setText("");

    }



    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.continueTV:
                if (validationOfData())
                {
                    HashMap<String, String> hashMap = new HashMap();
                    hashMap.put("username", userNameET.getText().toString());
                    hashMap.put("password", passwordET.getText().toString());
                    Constant.execute(new Super_AsyncTask(getActivity(), hashMap, Constant.LOGIN, new Super_AsyncTask_Interface() {

                        @Override
                        public void onTaskCompleted(String output) {
                            Log.d("login_op", "" + output);
                            try {
                                JSONObject jsonObject =new JSONObject(output);
                                if (jsonObject.optString("success").equals("false"))
                                {
                                    Constant.showAlert(getActivity(),"Alert",jsonObject.optString("message"));
                                }
                                else  if (jsonObject.optString("success").equals("true"))
                                {
                                    clearFields();
                                    MySharedPreferences mySharedPreferences=new MySharedPreferences();
                                    mySharedPreferences.storeData(getActivity(),"USER_ID",jsonObject.optString("user_id"));
                                    mySharedPreferences.storeData(getActivity(),"USER_NAME",jsonObject.optString("username"));
                                    mySharedPreferences.storeData(getActivity(),"SESSION","true");

                                    Intent fragment_activityy = new Intent(getActivity(), HomePage.class);
                                    startActivity(fragment_activityy);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }

                    }, true, false));
                }




               /* FragmentActivity.fragActivitytitleTV.setText("Signup");
                SignUpFragment signUpFragment = new SignUpFragment();
                getActivity().getSupportFragmentManager().beginTransaction()
                        .add(R.id.fragmentsContainer, signUpFragment)
                        .commit();*/


                break;
            case R.id.termsConditionsTV:
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragmentsContainer, new TermsConditions()).addToBackStack(null)
                        .commit();

               /* FragmentActivity.fragActivitytitleTV.setText("Signup");
                SignUpFragment signUpFragment = new SignUpFragment();
                getActivity().getSupportFragmentManager().beginTransaction()
                        .add(R.id.fragmentsContainer, signUpFragment)
                        .commit();*/


                break;
           /* case R.id.forgotPassTV:
                FragmentActivity.fragActivitytitleTV.setText("Forgot Password");
                ForgotPassword forgotPassword = new ForgotPassword();
                getActivity().getSupportFragmentManager().beginTransaction()
                        .add(R.id.fragmentsContainer, forgotPassword).addToBackStack(null)
                        .commit();
                break;
            case R.id.loginTV:
                HashMap<String, String> hashMap = new HashMap();
                hashMap.put("Email ID", userNameET.getText().toString());
                hashMap.put("Password", passwordET.getText().toString());
                Constant.execute(new Super_AsyncTask(getActivity(), hashMap, Constant.LOGIN_API, new Super_AsyncTask_Interface() {

                    @Override
                    public void onTaskCompleted(String output) {
                        Log.d("login_op", "" + output);

                    }

                }, true, false));


                break;*/
        }

    }
    private boolean validationOfData() {
        boolean status=true;
        if (!Constant.validateFields(userNameET.getText().toString())) {
            userNameET.setError("Please Enter Your Email-ID");
            status=false;
        } if (!Constant.validateFields(passwordET.getText().toString())) {
            passwordET.setError("Please Enter Password.");
            status=false;
        }




        return status;
    }

}
