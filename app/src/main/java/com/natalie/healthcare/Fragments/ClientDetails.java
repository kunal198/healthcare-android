package com.natalie.healthcare.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.natalie.healthcare.Activities.HomePage;
import com.natalie.healthcare.R;
import com.natalie.healthcare.Utills.Constant;

/**
 * Created by brst-pc20 on 1/6/17.
 */

public class ClientDetails extends Fragment implements View.OnClickListener {

    private String client_name = "", location = "", client_id = "";
    private TextView carePlanTV, clientNameTV, medicationChartTV, locationTV, client_support_planTV;

    public ClientDetails() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Constant.hideKeyboard(getActivity());
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.client_details, container, false);
        initView(view);


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        HomePage.fragActivitytitleTV.setText("CLIENT DETAILS");
        ((HomePage) getActivity()).addIV.setVisibility(View.GONE);
    }

    private void initView(View view) {
        Bundle bundle = getArguments();
        if (bundle != null) {
            client_name = bundle.getString("client_name");
            location = bundle.getString("location");
            client_id = bundle.getString("client_id");


        }

        carePlanTV = (TextView) view.findViewById(R.id.carePlanTV);
        clientNameTV = (TextView) view.findViewById(R.id.clientNameTV);
        medicationChartTV = (TextView) view.findViewById(R.id.medicationChartTV);
        client_support_planTV = (TextView) view.findViewById(R.id.client_support_planTV);
        locationTV = (TextView) view.findViewById(R.id.locationTV);
        clientNameTV.setText(client_name);
        locationTV.setText(location);


        medicationChartTV.setOnClickListener(this);
        carePlanTV.setOnClickListener(this);
        client_support_planTV.setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.addIV:
                CarePlan carePlan = new CarePlan();
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragmentsContainer2, carePlan).addToBackStack(null)
                        .commit();

                break;
            case R.id.medicationChartTV:
                doTransaction("medication_chart");
                break;
            case R.id.carePlanTV:
                doTransaction("care_plan");

                break;
            case R.id.client_support_planTV:
                doTransaction("client_support_plan");
                break;

        }

    }

    private void doTransaction(String value) {
        MedicationPlanDetails medicationPlanDetails = new MedicationPlanDetails();
        Bundle bundlee = getArguments();
        bundlee.putString("path", value);
        bundlee.putString("client_name", client_name);
        medicationPlanDetails.setArguments(bundlee);
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragmentsContainer2, medicationPlanDetails).addToBackStack(null)
                .commit();
    }
}
