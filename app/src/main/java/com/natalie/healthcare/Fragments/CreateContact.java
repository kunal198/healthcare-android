package com.natalie.healthcare.Fragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.seatgeek.placesautocomplete.DetailsCallback;
import com.seatgeek.placesautocomplete.OnPlaceSelectedListener;
import com.seatgeek.placesautocomplete.PlacesAutocompleteTextView;
import com.seatgeek.placesautocomplete.model.AutocompleteResultType;
import com.seatgeek.placesautocomplete.model.PlaceDetails;
import com.natalie.healthcare.Activities.HomePage;
import com.natalie.healthcare.Adapters.ClientsAdapter;
import com.natalie.healthcare.Async_Thread.Super_AsyncTask;
import com.natalie.healthcare.Async_Thread.Super_AsyncTask_Interface;
import com.natalie.healthcare.Bean.ClientsData;
import com.natalie.healthcare.CallBacks.CallBacks;
import com.natalie.healthcare.PlacesDetails;
import com.natalie.healthcare.R;
import com.natalie.healthcare.SharedPreferences.MySharedPreferences;
import com.natalie.healthcare.Utills.Constant;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by brst-pc20 on 1/6/17.
 */

public class CreateContact extends Fragment implements View.OnClickListener, CallBacks {
    String location = "";
    double lat, lng;
    EditText firstNameET, lastNameET, telephoneET, doorCodesET;
    TextView createTV;
    ClientsAdapter clientsAdapter;
    ArrayList<ClientsData> data_list;
    View view;
    CallBacks callBacks;
    public static PlacesAutocompleteTextView clientLocationTV;
    private LocationManager locationManager;

    public CreateContact() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Constant.hideKeyboard(getActivity());
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.create_clients, container, false);
            initView(view);
        }
        ((HomePage) getActivity()).logoutTV.setVisibility(View.GONE);
        ((HomePage) getActivity()).addIV.setVisibility(View.GONE);
        ((HomePage) getActivity()).backIV.setVisibility(View.GONE);

       /* if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.create_clients, container, false);
            initView(view);
        } catch (InflateException e) {
        *//* map is already there, just return view as it is *//*
        }*/
        return view;
    }

    private void initView(View view) {
        // locationTV = (PlacesAutocompleteTextView) view.findViewById(R.id.locationTV);
        callBacks = this;
        firstNameET = (EditText) view.findViewById(R.id.firstNameET);
        lastNameET = (EditText) view.findViewById(R.id.lastNameET);
        telephoneET = (EditText) view.findViewById(R.id.telephoneET);
        doorCodesET = (EditText) view.findViewById(R.id.doorCodesET);
        clientLocationTV = (PlacesAutocompleteTextView) view.findViewById(R.id.clientLocationTV);
        locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);

        createTV = (TextView) view.findViewById(R.id.createTV);
        data_list = new ArrayList<>();
        //clientsAdapter=new ClientsAdapter(getActivity(),data_list);
        createTV.setOnClickListener(this);
        clientLocationTV.setResultType(AutocompleteResultType.GEOCODE);
        clientLocationTV.setHistoryManager(null);
        clientLocationTV.setRadiusMeters(Long.valueOf(1000));
        accessLocation();
      /*  AutoCompleteTextView autocompleteView = (AutoCompleteTextView) view.findViewById(R.id.autocomplete);
        autocompleteView.setAdapter(new PlacesAutoCompleteAdapter(getActivity(), R.layout.autocomplete_text_item));*/
        clientLocationTV.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE) {
                    // do your stuff here
                    Constant.hideKeyboard(getActivity());
                }
                return false;
            }
        });

        clientLocationTV.setOnPlaceSelectedListener(new OnPlaceSelectedListener() {
            @Override
            public void onPlaceSelected(@NonNull com.seatgeek.placesautocomplete.model.Place place) {
                Log.d("placee", "----" + place.description);
                location = place.description;
                clientLocationTV.getDetailsFor(place, new DetailsCallback() {
                    @Override
                    public void onSuccess(PlaceDetails placeDetails) {
                        Log.d("placee", "places");
                        //   client_name = "";
                        PlacesDetails pl = new PlacesDetails(getActivity());
                        ArrayList<Double> list = new ArrayList<Double>();
                        String data = pl.getLatLng(placeDetails.place_id, callBacks, "DESTINATION");
                        Log.d("locationFromPlaceid", "--" + data);
                    }

                    @Override
                    public void onFailure(Throwable throwable) {

                    }
                });
            }
        });



/*
* The following code example shows setting an AutocompleteFilter on a PlaceAutocompleteFragment to
* set a filter returning only results with a precise address.
*/

    }


    private void accessLocation() {

        Location location = getLastKnownLocation();
        if (location != null) {
            clientLocationTV.setCurrentLocation(location);
        }
    }
    private Location getLastKnownLocation() {
        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.

                bestLocation = locationManager.getLastKnownLocation(provider);
                //   Toast.makeText(getActivity(), "helo" + bestLocation, Toast.LENGTH_SHORT).show();
                Log.d("hello", "helllo" + bestLocation);


                Location l = locationManager.getLastKnownLocation(provider);

                //resetLocation();

                if (l == null) {
                    continue;
                }
                if (bestLocation == null
                        || l.getAccuracy() < bestLocation.getAccuracy()) {

                    bestLocation = l;
                }
            } else {
                bestLocation = locationManager.getLastKnownLocation(provider);
                //  Toast.makeText(getActivity(), "helo" + bestLocation, Toast.LENGTH_SHORT).show();
                Log.d("hello", "helllo" + bestLocation);
                Location l = locationManager.getLastKnownLocation(provider);

                if (l == null) {
                    continue;
                }
                if (bestLocation == null
                        || l.getAccuracy() < bestLocation.getAccuracy()) {

                    bestLocation = l;
                }
            }
            if (bestLocation == null) {
                return null;
            }


        }
        return bestLocation;
    }
    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.createTV:

/*
                        PlaceAPI placeAPI=new PlaceAPI();
                       placeAPI.autocomplete("mohali");*/

                if (validationOfData()) {


                    String user_id = MySharedPreferences.getInstance().getData(getActivity(), "USER_ID");
                    HashMap<String, String> hashMap = new HashMap();
                    hashMap.put("fname", firstNameET.getText().toString());
                    hashMap.put("lname", lastNameET.getText().toString());
                    hashMap.put("telephone", telephoneET.getText().toString());
                    hashMap.put("dcode", doorCodesET.getText().toString());
                    hashMap.put("loc", location);
                    hashMap.put("uid", user_id);
                    hashMap.put("lat", String.valueOf(lat));
                    hashMap.put("long", String.valueOf(lng));
                    Constant.execute(new Super_AsyncTask(getActivity(), hashMap, Constant.CREATE_CLIENT, new Super_AsyncTask_Interface() {

                        @Override
                        public void onTaskCompleted(String output) {
                            Log.d("crete_client", "" + output);
                            try {
                                JSONObject jsonObject = new JSONObject(output);

                                if (jsonObject.optString("success").equals("true")) {


                                    firstNameET.setText("");
                                    lastNameET.setText("");
                                    telephoneET.setText("");
                                    doorCodesET.setText("");
                                    clientLocationTV.setText("");

                                    ((HomePage)getActivity()).onBackPressed();

                                } else {
                                    Constant.showAlert(getActivity(), "Alert", jsonObject.optString("message"));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                          /*  try {
                                JSONObject jsonObject =new JSONObject(output);
                                if (jsonObject.optString("success").equals("false"))
                                {
                                    Constant.showAlert(getActivity(),"Alert",jsonObject.optString("message"));
                                }
                                else  if (jsonObject.optString("success").equals("true"))
                                {
                                    Intent fragment_activityy = new Intent(getActivity(), HomePage.class);
                                    startActivity(fragment_activityy);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
*/

                        }

                    }, true, false));


                }


                break;

        }

    }
    private void replaceFragment (Fragment fragment){
        String backStateName = fragment.getClass().getName();

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate (backStateName, 0);

        if (!fragmentPopped){ //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.fragmentsContainer2, fragment);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }


    private boolean validationOfData() {
        boolean status = true;
        if (!Constant.validateFields(firstNameET.getText().toString())) {
            firstNameET.setError("Please Enter Your Name.");
            status = false;
        }
        if (!Constant.validateFields(lastNameET.getText().toString())) {
            lastNameET.setError("Please Enter Your  Last Name.");
            status = false;
        }
        if (!Constant.validateFields(telephoneET.getText().toString())) {
            telephoneET.setError("Please Enter Your  Telephone Number.");
            status = false;
        }
        if (!Constant.validateFields(doorCodesET.getText().toString())) {
            doorCodesET.setError("Please Enter Door Codes.");
            status = false;
        }
        if (!Constant.validateFields(location)) {
            Constant.showAlert(getActivity(), "Alert", "Pleaase Enter the Location");

            status = false;
        }


        return status;
    }

    @Override
    public void latLngFromPlaceId(String data, String origin) {
        try {
            JSONObject jsonObject = new JSONObject(data);
            JSONObject jsonObject1 = jsonObject.optJSONObject("result").optJSONObject("geometry").optJSONObject("location");

            lat = Double.parseDouble(jsonObject1.optString("lat"));
            lng = Double.parseDouble(jsonObject1.optString("lng"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
