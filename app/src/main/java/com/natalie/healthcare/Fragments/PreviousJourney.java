package com.natalie.healthcare.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.natalie.healthcare.Activities.HomePage;
import com.natalie.healthcare.Adapters.PreviousJourneyAdapter;
import com.natalie.healthcare.Bean.RealmDataBeans;
import com.natalie.healthcare.R;
import com.natalie.healthcare.RealmDataBase.RealmController;
import com.natalie.healthcare.Utills.Constant;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by brst-pc20 on 1/6/17.
 */

public class PreviousJourney extends Fragment implements View.OnClickListener {

    RecyclerView previousJourneyRV;
    PreviousJourneyAdapter previousJourneyAdapter;
    private Realm realm;

    public PreviousJourney() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Constant.hideKeyboard(getActivity());
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.previous_journeys, container, false);
        realm= RealmController.with(this).getRealm();
        initView(view);
        ((HomePage)getActivity()).logoutTV.setVisibility(View.GONE);
        ((HomePage)getActivity()).addIV.setVisibility(View.GONE);
        ((HomePage)getActivity()).backIV.setVisibility(View.GONE);
        return view;
    }
    @Override
    public void onResume() {
        super.onResume();
        HomePage.fragActivitytitleTV.setText("Previous Journeys");
    }
    private void initView(View view)
    {
        RealmResults<RealmDataBeans> results = realm.where(RealmDataBeans.class).
                contains("sourceData","source")
                .or()
                .contains("destinationData","destination")
                .or()
                .contains("mapImagePath","path")
                .or()
                .contains("clockInTime","checkin")
                .or()
                .contains("clockOutTime","checkout")
                .findAll();

        previousJourneyRV= (RecyclerView) view.findViewById(R.id.previousJourneyRV);
        previousJourneyAdapter=new PreviousJourneyAdapter(getActivity(),results);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        previousJourneyRV.setLayoutManager(mLayoutManager);
        previousJourneyRV.setItemAnimator(new DefaultItemAnimator());
        previousJourneyRV.setAdapter(previousJourneyAdapter);


    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
        }

    }
}
