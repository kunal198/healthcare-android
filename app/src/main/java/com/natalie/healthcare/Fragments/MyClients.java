package com.natalie.healthcare.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.natalie.healthcare.Activities.HomePage;
import com.natalie.healthcare.Adapters.ClientsAdapter;
import com.natalie.healthcare.Bean.ClientsData;
import com.natalie.healthcare.CallBacks.GetClientsCallBack;
import com.natalie.healthcare.R;
import com.natalie.healthcare.Utills.Constant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by brst-pc20 on 1/6/17.
 */

public class MyClients extends Fragment implements View.OnClickListener, GetClientsCallBack {

    GetClientsCallBack getClientsCallBack;
    TextView noResultTV;
    LinearLayout noResultLL;
    RecyclerView clientRV;
    ClientsAdapter clientsAdapter;
    ArrayList<ClientsData> data_list;

    public MyClients() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Constant.hideKeyboard(getActivity());
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.my_clients, container, false);
        initView(view);
        ((HomePage) getActivity()).logoutTV.setVisibility(View.GONE);
        ((HomePage) getActivity()).addIV.setVisibility(View.VISIBLE);
        ((HomePage) getActivity()).backIV.setVisibility(View.GONE);
        ((HomePage) getActivity()).addIV.setOnClickListener(this);
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        HomePage.fragActivitytitleTV.setText("My Clients");
        if (CreateContact.clientLocationTV!=null)
        CreateContact.clientLocationTV.setText("");

    }

    private void initView(View view) {
        noResultTV = (TextView) view.findViewById(R.id.noResultTV);
        noResultLL = (LinearLayout) view.findViewById(R.id.noResultLL);

        clientRV = (RecyclerView) view.findViewById(R.id.clientRV);
        data_list = new ArrayList<>();

        getClientsCallBack=this;
        clientsAdapter = new ClientsAdapter(getActivity(), data_list, MyClients.this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        clientRV.setLayoutManager(mLayoutManager);
        clientRV.setItemAnimator(new DefaultItemAnimator());
        clientRV.setAdapter(clientsAdapter);

        Constant.getCLients(getActivity(),getClientsCallBack);

        // passwordET = (TextView) view.findViewById(R.id.passwordET);
        //  userNameET = (TextView) view.findViewById(R.id.userNameET);
        // loginTV = (TextView) view.findViewById(R.id.loginTV);
        //  forgotPassTV = (TextView) view.findViewById(R.id.forgotPassTV);
        //  signUpTV = (TextView) view.findViewById(R.id.signUpTV);
        //   signUpTV.setOnClickListener(this);
        //  forgotPassTV.setOnClickListener(this);
        //   loginTV.setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.addIV:
                CreateContact createContact = new CreateContact();
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragmentsContainer2, createContact).addToBackStack("myclient")
                        .commit();

                break;

        }

    }

    @Override
    public void onResult(String data) {
        Log.d("get_clients", "" + data);
        if (!data.isEmpty())
        {

        try {
            JSONObject jsonObject = new JSONObject(data);
            if (jsonObject.optString("success").equals("false")) {
                clientRV.setVisibility(View.GONE);
                noResultLL.setVisibility(View.VISIBLE);
                // Constant.showAlert(getActivity(),"Alert",jsonObject.optString("message"));
            } else if (jsonObject.optString("success").equals("true")) {
                clientRV.setVisibility(View.VISIBLE);
                noResultLL.setVisibility(View.GONE);
                JSONArray jsonArray1 = jsonObject.optJSONArray("data");
                for (int i = 0; i < jsonArray1.length(); i++) {
                    ClientsData clientsData = new ClientsData();
                    JSONObject jsonObject1 = jsonArray1.optJSONObject(i);
                    clientsData.setClientName(jsonObject1.optString("name"));
                    clientsData.setClientTelephoneNo(jsonObject1.optString("Telephone Number"));
                    clientsData.setClientDoorCodes(jsonObject1.optString("Door Codes"));
                    clientsData.setClientLocation(jsonObject1.optString("location"));
                    clientsData.setClientLattitude(jsonObject1.optString("latitude"));
                    clientsData.setClientLongitude(jsonObject1.optString("longitude"));
                    clientsData.setClientId(jsonObject1.optString("client_id"));
                    data_list.add(clientsData);
                }
                clientsAdapter.notifyDataSetChanged();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    }

}
