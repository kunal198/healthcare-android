package com.natalie.healthcare.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.natalie.healthcare.Activities.HomePage;
import com.natalie.healthcare.Adapters.WorkDiaryAdapter;
import com.natalie.healthcare.Async_Thread.Super_AsyncTask;
import com.natalie.healthcare.Async_Thread.Super_AsyncTask_Interface;
import com.natalie.healthcare.Bean.ClientsData;
import com.natalie.healthcare.R;
import com.natalie.healthcare.SharedPreferences.MySharedPreferences;
import com.natalie.healthcare.Utills.Constant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by brst-pc20 on 1/6/17.
 */

public class WorkDiary extends Fragment implements View.OnClickListener {
    TextView noResultTV;
    LinearLayout noResultLL;
    RecyclerView client_listRV;
    WorkDiaryAdapter workDiaryAdapter;
    ArrayList<ClientsData> data_list;
    public WorkDiary() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Constant.hideKeyboard(getActivity());
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.work_diary, container, false);
        initView(view);
        ((HomePage) getActivity()).logoutTV.setVisibility(View.GONE);
        ((HomePage) getActivity()).addIV.setVisibility(View.GONE);
        ((HomePage) getActivity()).backIV.setVisibility(View.GONE);
        return view;
    }

    private void initView(View view) {

        client_listRV = (RecyclerView) view.findViewById(R.id.client_listRV);
        noResultTV = (TextView) view.findViewById(R.id.noResultTV);
        noResultLL = (LinearLayout) view.findViewById(R.id.noResultLL);
      data_list = new ArrayList<>();


        workDiaryAdapter = new WorkDiaryAdapter(getActivity(), data_list);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        client_listRV.setLayoutManager(mLayoutManager);
        client_listRV.setItemAnimator(new DefaultItemAnimator());
        client_listRV.setAdapter(workDiaryAdapter);
        getAssignedTasks();
    }
    @Override
    public void onResume() {
        super.onResume();
        HomePage.fragActivitytitleTV.setText("Work Diary");
    }
    private void getAssignedTasks() {
        MySharedPreferences mySharedPreferences = new MySharedPreferences();
        String user_id = mySharedPreferences.getData(getActivity(), "USER_ID");
        HashMap hashMap = new HashMap();
        hashMap.put("user_id", user_id);
        Log.d("user_id", "" + user_id);
        Constant.execute(new Super_AsyncTask(getActivity(), hashMap, Constant.GET_ASSIGNED_TASK, new Super_AsyncTask_Interface() {
            @Override
            public void onTaskCompleted(String result) {

                Log.d("getAssignedTasks", "result" + result);
                try {
                    JSONObject jsonObject=new JSONObject(result);
                    JSONArray jsonArray=jsonObject.optJSONArray("clients");
                    if (jsonArray!=null)
                    {
                    client_listRV.setVisibility(View.VISIBLE);
                    noResultLL.setVisibility(View.GONE);
                    for (int i=0;i<jsonArray.length();i++)
                    {
                        ClientsData clientsData=new ClientsData();
                        JSONObject jsonObject1=jsonArray.optJSONObject(i);
                        clientsData.setClientName(jsonObject1.optString("firstname")+" "+jsonObject1.optString("lastname"));
                        clientsData.setClientTelephoneNo(jsonObject1.optString("telephone"));
                        clientsData.setClientDoorCodes(jsonObject1.optString("door_codes"));
                        clientsData.setClientLocation(jsonObject1.optString("location"));
                        clientsData.setClientLattitude(jsonObject1.optString("latitude"));
                        clientsData.setClientLongitude(jsonObject1.optString("longitude"));
                        clientsData.setStartTime(jsonObject1.optString("start_time"));
                        clientsData.setEndTime(jsonObject1.optString("end_time"));
                        clientsData.setWorkingHour(jsonObject1.optString("duration"));
                        data_list.add(clientsData);

                    }

                    }
                    else{
                        client_listRV.setVisibility(View.GONE);
                        noResultLL.setVisibility(View.VISIBLE);
                        noResultTV.setText(jsonObject.optString("message"));
                    }
                    workDiaryAdapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, true, false));


    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {


        }

    }
}
