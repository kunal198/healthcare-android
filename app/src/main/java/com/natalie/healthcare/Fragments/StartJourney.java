package com.natalie.healthcare.Fragments;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.hardware.Camera;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.natalie.healthcare.Activities.HomePage;
import com.natalie.healthcare.CallBacks.CallBacks;
import com.natalie.healthcare.CallBacks.GetClientsCallBack;
import com.natalie.healthcare.Dialog.AddNote;
import com.natalie.healthcare.DirectionsJSONParser;
import com.natalie.healthcare.PlacesDetails;
import com.natalie.healthcare.R;
import com.natalie.healthcare.RealmDataBase.RealmController;
import com.natalie.healthcare.Utills.Constant;
import com.natalie.healthcare.Utills.LocationAddress;
import com.seatgeek.placesautocomplete.DetailsCallback;
import com.seatgeek.placesautocomplete.OnPlaceSelectedListener;
import com.seatgeek.placesautocomplete.PlacesAutocompleteTextView;
import com.seatgeek.placesautocomplete.model.AutocompleteResultType;
import com.seatgeek.placesautocomplete.model.Place;
import com.seatgeek.placesautocomplete.model.PlaceDetails;
import com.shehabic.droppy.DroppyClickCallbackInterface;
import com.shehabic.droppy.DroppyMenuItem;
import com.shehabic.droppy.DroppyMenuPopup;
import com.shehabic.droppy.animations.DroppyFadeInAnimation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import io.realm.Realm;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by brst-pc20 on 1/6/17.
 */

public class StartJourney extends Fragment implements View.OnClickListener, CallBacks, GetClientsCallBack {

    DroppyMenuPopup droppyMenu;
    GetClientsCallBack clientsCallBack;
    String image_path = "";
    Location current_location;
    String check_in_time = "", check_out_time = "", source_address = "", destination_address = "", client_name = "", client_id = "";
    String destination_lat = "", destination_lng = "", source_lng = "", source_lat = "";
    TextView startJourneyTV, timeTV, distanceTV;
    public TextView checkInTV;
    public static String time = "", distance = "";
    CallBacks callBacks;
    ImageView srcIV, destinationIV;
    public static PlacesAutocompleteTextView sourceTV, destinationTV;
    View view;
    public static boolean isNeedToshowAddButton = false;
    private LocationManager locationManager;

    private GoogleMap googleMap;
    Camera camera;
    Realm realm;

    public StartJourney() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Constant.hideKeyboard(getActivity());
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.my_journey, container, false);
            initView(view);
        }
        ((HomePage) getActivity()).logoutTV.setVisibility(View.GONE);
        ((HomePage) getActivity()).addIV.setVisibility(View.GONE);
        ((HomePage) getActivity()).backIV.setVisibility(View.GONE);
        ((HomePage) getActivity()).addIV.setOnClickListener(this);
        return view;
    }


    private void initView(View view) {


        callBacks = this;
        clientsCallBack = this;
        sourceTV = (PlacesAutocompleteTextView) view.findViewById(R.id.sourceTV);
        destinationTV = (PlacesAutocompleteTextView) view.findViewById(R.id.destinationTV);
        srcIV = (ImageView) view.findViewById(R.id.srcIV);
        destinationIV = (ImageView) view.findViewById(R.id.destinationIV);
        startJourneyTV = (TextView) view.findViewById(R.id.startJourneyTV);
        sourceTV.setResultType(AutocompleteResultType.GEOCODE);
        destinationTV.setResultType(AutocompleteResultType.GEOCODE);
        sourceTV.setHistoryManager(null);

        destinationTV.setHistoryManager(null);

        realm = RealmController.with(this).getRealm();
        checkInTV = (TextView) view.findViewById(R.id.checkInTV);
        timeTV = (TextView) view.findViewById(R.id.timeTV);
        distanceTV = (TextView) view.findViewById(R.id.distanceTV);
        locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);

        srcIV.setOnClickListener(this);
        destinationIV.setOnClickListener(this);
        checkInTV.setOnClickListener(this);
        startJourneyTV.setOnClickListener(this);
        initilizeMap();
        accessLocation();
        Constant.getCLients(getActivity(), clientsCallBack);


        destinationTV.setOnPlaceSelectedListener(new OnPlaceSelectedListener() {
            @Override
            public void onPlaceSelected(@NonNull Place place) {
                Log.d("placee", "----" + place.description);
                destination_address = place.description;
                destinationTV.getDetailsFor(place, new DetailsCallback() {
                    @Override
                    public void onSuccess(PlaceDetails placeDetails) {
                        Log.d("placee", "places");
                        client_name = "";
                        PlacesDetails pl = new PlacesDetails(getActivity());
                        ArrayList<Double> list = new ArrayList<Double>();
                        String data = pl.getLatLng(placeDetails.place_id, callBacks, "DESTINATION");
                        Log.d("locationFromPlaceid", "--" + data);
                    }

                    @Override
                    public void onFailure(Throwable throwable) {

                    }
                });
            }
        });


        sourceTV.setOnPlaceSelectedListener(new OnPlaceSelectedListener() {
            @Override
            public void onPlaceSelected(@NonNull Place place) {
                Log.d("placee", "----" + place.description);
                source_address = place.description;
                sourceTV.getDetailsFor(place, new DetailsCallback() {
                    @Override
                    public void onSuccess(PlaceDetails placeDetails) {
                        Log.d("placee", "places");

                        PlacesDetails pl = new PlacesDetails(getActivity());
                        ArrayList<Double> list = new ArrayList<Double>();
                        String data = pl.getLatLng(placeDetails.place_id, callBacks, "SOURCE");
                        Log.d("locationFromPlaceid", "--" + data);

                    }

                    @Override
                    public void onFailure(Throwable throwable) {

                    }
                });
            }
        });
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            destination_lat = bundle.getString("lat");
            destination_lng = bundle.getString("lng");
            client_name = bundle.getString("client_name");
            client_id = bundle.getString("client_id");
            destination_address = bundle.getString("address");
            destinationTV.setText(destination_address);
            startJourney();
        }

        sourceTV.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE) {
                    // do your stuff here
                    Constant.hideKeyboard(getActivity());
                }
                return false;
            }
        });
        destinationTV.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE) {
                    // do your stuff here
                    Constant.hideKeyboard(getActivity());
                }
                return false;
            }
        });
    }


    private void initilizeMap() {
        if (googleMap == null) {
            googleMap = ((SupportMapFragment) getChildFragmentManager().findFragmentById(
                    R.id.mapView)).getMap();



            // check if map is created successfully or not
            if (googleMap == null) {
                Toast.makeText(getActivity(),
                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.addIV:
                Bundle bundle = new Bundle();
                bundle.putString("client_name", client_name);
                bundle.putString("location", destination_address);
                bundle.putString("client_id", client_id);
                ClientDetails createContact = new ClientDetails();
                createContact.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragmentsContainer2, createContact).addToBackStack(null)
                        .commit();

                break;
            case R.id.srcIV:
                sourceTV.setText("");
                source_lat = "";
                source_lng = "";

                break;

            case R.id.destinationIV:
                destinationTV.setText("");


                //   droppyMenu.show();

                break;
            case R.id.checkInTV:
                if (checkInTV.getText().toString().trim().equalsIgnoreCase("Check In")) {
                    CaptureMapScreen();
                    Constant.showAlert(getActivity(), "Alert", "You are successfully Checked In");
                    checkInTV.setText("Check Out");
                    if (!client_name.isEmpty()) {
                        ((HomePage) getActivity()).addIV.setVisibility(View.VISIBLE);
                        isNeedToshowAddButton = true;
                    }
                    DateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy,hh:mm a");
                    dateFormatter.setLenient(false);
                    Date today = new Date();
                    check_in_time = dateFormatter.format(today);
                } else if (checkInTV.getText().toString().trim().equalsIgnoreCase("Check Out")) {

                    //  Constant.showAlert(getActivity(),"Alert","");
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Alert")
                            .setMessage("Are you sure want to Check Out?")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    ((HomePage) getActivity()).addIV.setVisibility(View.GONE);
                                    DateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy,hh:mm a");
                                    dateFormatter.setLenient(false);
                                    Date today = new Date();
                                    check_out_time = dateFormatter.format(today);

                                    //  check_out_time = DateFormat.getDateTimeInstance().format(new Date());

                                    AddNote addNote = new AddNote(StartJourney.this, getActivity(), client_name, source_address, destination_address, check_in_time, check_out_time, image_path, realm);
                                    addNote.show();
                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();

                }

                Log.d("log_tag", "-calendar-" + check_in_time);


                break;
            case R.id.startJourneyTV:

                if (!source_lat.isEmpty() && !source_lng.isEmpty() && !destination_lat.isEmpty() && !destination_lng.isEmpty()) {
                    startJourney();

                } else {

                    if (source_lat.isEmpty()) {
                        Constant.showAlert(getActivity(), "Alert", "Please choose Starting point.");

                    } else {
                        Constant.showAlert(getActivity(), "Alert", "Please choose destination.");

                    }

                }

                break;


        }

    }


    private void startJourney() {
        if (!source_lat.isEmpty() && !source_lng.isEmpty()&&!destination_lat.isEmpty() && !destination_lng.isEmpty()) {


            String url = getDirectionsUrl(new LatLng(Double.parseDouble(source_lat), Double.parseDouble(source_lng)), new LatLng(Double.parseDouble(destination_lat), Double.parseDouble(destination_lng)));
            DownloadTask downloadTask = new DownloadTask();

            // Start downloading json data from Google Directions API
            downloadTask.execute(url);
        }
    }

    public void CaptureMapScreen() {
        GoogleMap.SnapshotReadyCallback callback = new GoogleMap.SnapshotReadyCallback() {
            Bitmap bitmap;

            @Override
            public void onSnapshotReady(Bitmap snapshot) {
                // TODO Auto-generated method stub
                bitmap = snapshot;
                try {
                    image_path = "/mnt/sdcard/"
                            + "MyMapScreen" + System.currentTimeMillis()
                            + ".png";
                    FileOutputStream out = new FileOutputStream(image_path);

                    // above "/mnt ..... png" => is a storage path (where image will be stored) + name of image you can customize as per your Requirement

                    bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        googleMap.snapshot(callback);

        // myMap is object of GoogleMap +> GoogleMap myMap;
        // which is initialized in onCreate() =>
        // myMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_pass_home_call)).getMap();
    }

    @Override
    public void onResult(String data) {
        JSONArray jsonArray = null;
        Log.d("dataaa", "---" + data);
        DroppyMenuPopup.Builder droppyBuilder = new DroppyMenuPopup.Builder(getActivity(), destinationIV);

        try {
            JSONObject jsonObject = new JSONObject(data);
            jsonArray = jsonObject.optJSONArray("data");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject1 = jsonArray.optJSONObject(i);

                droppyBuilder.addMenuItem(new DroppyMenuItem(jsonObject1.optString("name") + "\n" + jsonObject1.optString("location")))
                        .addSeparator();

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final JSONArray finalJsonArray = jsonArray;
        droppyBuilder.setOnClick(new DroppyClickCallbackInterface() {
            @Override
            public void call(View v, int id) {
                //sourceTV.setText();
                Log.d("Clicked on ", String.valueOf(id));
                destination_lat = finalJsonArray.optJSONObject(id).optString("latitude");
                destination_lng = finalJsonArray.optJSONObject(id).optString("longitude");
                destination_address = finalJsonArray.optJSONObject(id).optString("location");
                destinationTV.setText(destination_address);
                client_name = finalJsonArray.optJSONObject(id).optString("name");
                client_id = finalJsonArray.optJSONObject(id).optString("client_id");
                Log.d("client_id", "----" + client_id);
            }
        });
        droppyBuilder.setPopupAnimation(new DroppyFadeInAnimation())
                .setXOffset(50);
        droppyMenu = droppyBuilder.build();


    }


    /**
     * A class to download data from Google Directions URL
     */
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            Log.d("log_tag", "onPostExecute" + result.toString());
            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

    /**
     * A class to parse the Google Directions in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {

            Log.d("log_tag", "result" + result.toString());
            if (result.size() > 0) {


                ArrayList<LatLng> points = null;
                PolylineOptions lineOptions = null;

                // Traversing through all the routes
                for (int i = 0; i < result.size(); i++) {
                    points = new ArrayList<LatLng>();
                    lineOptions = new PolylineOptions();

                    // Fetching i-th route
                    List<HashMap<String, String>> path = result.get(i);

                    // Fetching all the points in i-th route
                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);

                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                     /*   distanceTV.setText(point.get("distance"));
                        timeTV.setText(point.get("duration"));*/
                        LatLng position = new LatLng(lat, lng);

                        points.add(position);

                    }
                    // Adding all the points in the route to LineOptions
                    lineOptions.addAll(points);
                    lineOptions.width(8);
                    lineOptions.color(Color.RED);
                }
                checkInTV.setVisibility(View.VISIBLE);
                googleMap.clear();
                addMarker();
                timeTV.setText(time);
                distanceTV.setText("Distance: " + distance);
                LatLngBounds.Builder builder = new LatLngBounds.Builder();


                builder.include(new LatLng(Double.parseDouble(source_lat), Double.parseDouble(source_lng)));
                builder.include(new LatLng(Double.parseDouble(destination_lat), Double.parseDouble(destination_lng)));
                LatLngBounds bounds = builder.build();
                googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 200));

                // Drawing polyline in the Google Map for the i-th route
                googleMap.addPolyline(lineOptions);


            } else {
                Constant.showAlert(getActivity(), "Alert", "Sorry.No route found");
            }
        }
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Exception ", "--" + e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    @Override
    public void onResume() {
        super.onResume();
        HomePage.fragActivitytitleTV.setText("My Journey");
        if (isNeedToshowAddButton)
            ((HomePage) getActivity()).addIV.setVisibility(View.VISIBLE);


    }


    private void accessLocation() {

        Location location = getLastKnownLocation();
        if (location != null) {
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            current_location = location;
            source_lng = String.valueOf(longitude);
            source_lat = String.valueOf(latitude);
            sourceTV.setCurrentLocation(location);
            destinationTV.setCurrentLocation(location);
            sourceTV.setRadiusMeters(Long.valueOf(500));
            destinationTV.setRadiusMeters(Long.valueOf(500));
            MarkerOptions marker = new MarkerOptions().position(new LatLng(latitude, longitude));
// adding marker

            googleMap.addMarker(marker);
            CameraPosition cameraPosition = new CameraPosition.Builder().target(
                    new LatLng(latitude, longitude)).zoom(12).build();

            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            LocationAddress locationAddress = new LocationAddress();
            locationAddress.getAddressFromLocation(latitude, longitude,
                    getActivity(), new GeocoderHandler());
        } else {
            // accessLocation();
        }
    }


    @Override
    public void latLngFromPlaceId(String data, String origin) {
        Log.d("log_tag", "dataa" + data);
        try {
            JSONObject jsonObject = new JSONObject(data);
            JSONObject jsonObject1 = jsonObject.optJSONObject("result").optJSONObject("geometry").optJSONObject("location");
            if (origin.equals("SOURCE")) {
                source_lat = jsonObject1.optString("lat");
                source_lng = jsonObject1.optString("lng");
            } else {

                destination_lat = jsonObject1.optString("lat");
                destination_lng = jsonObject1.optString("lng");

            }
            Log.d("log_tag", "source_lat" + source_lat + "source_lng" + source_lng + "destination_lat" + destination_lat + "destination_lng" + destination_lng);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void addMarker() {
        MarkerOptions marker1 = new MarkerOptions().position(new LatLng(Double.parseDouble(source_lat), Double.parseDouble(source_lng)));
        googleMap.addMarker(marker1);
        MarkerOptions marker = new MarkerOptions().position(new LatLng(Double.parseDouble(destination_lat), Double.parseDouble(destination_lng)));
        marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
// adding marker

        googleMap.addMarker(marker);
    }

    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String locationAddress;
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    locationAddress = bundle.getString("address");
                    break;
                default:
                    locationAddress = null;
            }
            source_address = locationAddress;
            sourceTV.setText(locationAddress);
        }
    }


    private Location getLastKnownLocation() {
        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.

                bestLocation = locationManager.getLastKnownLocation(provider);
                //   Toast.makeText(getActivity(), "helo" + bestLocation, Toast.LENGTH_SHORT).show();
                Log.d("hello", "helllo" + bestLocation);


                Location l = locationManager.getLastKnownLocation(provider);
                googleMap.setMyLocationEnabled(true);
                //resetLocation();

                if (l == null) {
                    continue;
                }
                if (bestLocation == null
                        || l.getAccuracy() < bestLocation.getAccuracy()) {

                    bestLocation = l;
                }
            } else {
                bestLocation = locationManager.getLastKnownLocation(provider);
                //  Toast.makeText(getActivity(), "helo" + bestLocation, Toast.LENGTH_SHORT).show();
                Log.d("hello", "helllo" + bestLocation);


                Location l = locationManager.getLastKnownLocation(provider);
                googleMap.setMyLocationEnabled(true);

                if (l == null) {
                    continue;
                }
                if (bestLocation == null
                        || l.getAccuracy() < bestLocation.getAccuracy()) {

                    bestLocation = l;
                }
            }
            if (bestLocation == null) {
                return null;
            }


        }
        return bestLocation;
    }

   /* private void resetLocation()
    {
        SupportMapFragment mapFragment = (SupportMapFragment)getActivity(). getSupportFragmentManager().
                findFragmentById(R.id.mapView);
        View mapView = mapFragment.getView();
        if (mapView != null &&
                mapView.findViewById(1) != null) {
            // Get the button view
            View locationButton = ((View) mapView.findViewById(1).getParent()).findViewById(2);
            // and next place it, on bottom right (as Google Maps app)
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)
                    locationButton.getLayoutParams();
            // position on right bottom
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
            layoutParams.setMargins(0, 0, 30, 30);
        }
    }*/


    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        return url;
    }


}//}