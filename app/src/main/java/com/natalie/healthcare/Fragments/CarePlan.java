package com.natalie.healthcare.Fragments;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import com.natalie.healthcare.Activities.HomePage;
import com.natalie.healthcare.Async_Thread.Super_AsyncTask;
import com.natalie.healthcare.Async_Thread.Super_AsyncTask_Interface;
import com.natalie.healthcare.Bean.MedicationPlans;
import com.natalie.healthcare.Bean.ObjectWrapperForBinder;
import com.natalie.healthcare.R;
import com.natalie.healthcare.Utills.Constant;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by brst-pc20 on 1/6/17.
 */

public class CarePlan extends Fragment implements View.OnClickListener, TimePickerDialog.OnTimeSetListener {

    EditText dateET, timeET1, timeET2, communicationET, foodET, urineET, bowlesET, printET, signatureET;
    TextView submitTV;
    Calendar myCalendar;
    String whichView = "", whichMode = "",plan_id="";
    int hour, minute;
    DatePickerDialog.OnDateSetListener date;
    private String client_id = "";

    public CarePlan() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Constant.hideKeyboard(getActivity());
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.care_plan, container, false);
        initView(view);




        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        HomePage.fragActivitytitleTV.setText("Care Plan");
        ((HomePage) getActivity()).addIV.setVisibility(View.GONE);

    }

    private void initView(View view) {
        dateET = (EditText) view.findViewById(R.id.dateET);
        timeET1 = (EditText) view.findViewById(R.id.timeET1);
        timeET2 = (EditText) view.findViewById(R.id.timeET2);
        communicationET = (EditText) view.findViewById(R.id.communicationET);
        foodET = (EditText) view.findViewById(R.id.foodET);
        urineET = (EditText) view.findViewById(R.id.urineET);
        bowlesET = (EditText) view.findViewById(R.id.bowlesET);
        printET = (EditText) view.findViewById(R.id.printET);
        signatureET = (EditText) view.findViewById(R.id.signatureET);
        submitTV = (TextView) view.findViewById(R.id.submitTV);

        submitTV.setOnClickListener(this);
        dateET.setOnClickListener(this);
        timeET1.setOnClickListener(this);
        timeET2.setOnClickListener(this);
        setDateDialog();
        Bundle bundle = getArguments();
        if (bundle != null) {

            client_id = bundle.getString("client_id");
            whichMode = "SAVE";
            submitTV.setVisibility(View.VISIBLE);
        }
        IBinder bundle1 = getArguments().getBinder("medical_plans");
        if (bundle1 != null) {
            whichMode = "EDIT";
            ((HomePage) getActivity()).logoutTV.setText("Edit");
            ((HomePage) getActivity()).logoutTV.setVisibility(View.VISIBLE);
            ((HomePage) getActivity()).logoutTV.setOnClickListener(CarePlan.this);
            submitTV.setVisibility(View.GONE);
            client_id = bundle.getString("client_id");
            MedicationPlans objReceived = ((ObjectWrapperForBinder) getArguments().getBinder("medical_plans")).getData();
            dateET.setText(objReceived.getDate());
            timeET1.setText(objReceived.getStart_time());
            timeET2.setText(objReceived.getEnd_time());
            communicationET.setText(objReceived.getCommunication());
            foodET.setText(objReceived.getFood());
            urineET.setText(objReceived.getUrine());
            bowlesET.setText(objReceived.getBowles());
            printET.setText(objReceived.getPrint());
            plan_id=objReceived.getId();
            client_id=objReceived.getClient_id();
            signatureET.setText(objReceived.getSignature());
            disableorEnableViews(false);
            Log.d("dataaaaa", "received object=" + objReceived);
        }


    }


    private void setDateDialog() {
        myCalendar = Calendar.getInstance();


        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        hour = myCalendar.get(Calendar.HOUR_OF_DAY);
        minute = myCalendar.get(Calendar.MINUTE);

        // Create a new instance of TimePickerDialog and return it

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.submitTV:
                if (whichMode.equals("SAVE")) {


                    if (validationOfData()) {
                        HashMap<String, String> hashMap = new HashMap();
                        hashMap.put("user_id", Constant.USER_ID);
                        hashMap.put("client_id", client_id);
                        hashMap.put("date", dateET.getText().toString());
                        hashMap.put("start_time", timeET1.getText().toString());
                        hashMap.put("end_time", timeET2.getText().toString());
                        hashMap.put("communication", communicationET.getText().toString());
                        hashMap.put("food", foodET.getText().toString());
                        hashMap.put("urine", urineET.getText().toString());
                        hashMap.put("bowles", bowlesET.getText().toString());
                        hashMap.put("print", printET.getText().toString());
                        hashMap.put("signature", signatureET.getText().toString());
                        Log.d("hashmapppp", "--" + hashMap);
                        Constant.execute(new Super_AsyncTask(getActivity(), hashMap, Constant.SAVE_CARE_PLANS, new Super_AsyncTask_Interface() {

                            @Override
                            public void onTaskCompleted(String output) {
                                Log.d("save_care_plans", "" + output);
                                try {
                                    JSONObject jsonObject = new JSONObject(output);
                                    String status = jsonObject.optString("status");
                                    String msg = jsonObject.optString("message");
                                    if (status.equals("success"))
                                    {
                                        resetFields();
                                        Constant.showAlert(getActivity(), "Alert", msg);

                                    }
                                    else{

                                        Constant.showAlert(getActivity(), "Alert", msg);
                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                            }

                        }, true, false));
                    }  }else {
                        if (validationOfData()) {
                            HashMap<String, String> hashMap = new HashMap();
                            hashMap.put("user_id", Constant.USER_ID);
                            hashMap.put("client_id", client_id);
                            hashMap.put("date", dateET.getText().toString());
                            hashMap.put("start_time", timeET1.getText().toString());
                            hashMap.put("end_time", timeET2.getText().toString());
                            hashMap.put("communication", communicationET.getText().toString());
                            hashMap.put("food", foodET.getText().toString());
                            hashMap.put("urine", urineET.getText().toString());
                            hashMap.put("bowles", bowlesET.getText().toString());
                            hashMap.put("print", printET.getText().toString());
                            hashMap.put("signature", signatureET.getText().toString());
                            hashMap.put("plan_id", plan_id);
                            Log.d("hashmapppp", "--" + hashMap);
                            Constant.execute(new Super_AsyncTask(getActivity(), hashMap, Constant.UPDATE_CARE_PLANS, new Super_AsyncTask_Interface() {

                                @Override
                                public void onTaskCompleted(String output) {
                                    Log.d("update_care_plans", "" + output);
                                    try {
                                        JSONObject jsonObject = new JSONObject(output);
                                        String status = jsonObject.optString("status");
                                        String msg = jsonObject.optString("message");

                                        if (status.equals("success"))
                                        {
                                            Constant.showAlert(getActivity(), "Alert", msg);
                                            submitTV.setVisibility(View.GONE);
                                        }
                                        else{

                                            Constant.showAlert(getActivity(), "Alert", msg);
                                        }


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                }

                            }, true, false));
                        }

                    }

                    break;
                    case R.id.logoutTV:
                        submitTV.setVisibility(View.VISIBLE);
                        disableorEnableViews(true);

                        break;
                    case R.id.dateET:


                        new DatePickerDialog(getActivity(), date, myCalendar
                                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                                myCalendar.get(Calendar.DAY_OF_MONTH)).show();

                        break;
                    case R.id.timeET1:
                        new TimePickerDialog(getActivity(), this, hour, minute,
                                DateFormat.is24HourFormat(getActivity())).show();
                        whichView = "time1";
                        break;
                    case R.id.timeET2:
                        whichView = "time2";
                        new TimePickerDialog(getActivity(), this, hour, minute,
                                DateFormat.is24HourFormat(getActivity())).show();
                        break;


                }

        }

    private boolean validationOfData() {
        boolean status = true;
        if (!Constant.validateFields(dateET.getText().toString())) {
            dateET.setError("Please Enter Date.");
            status = false;
        }
        if (!Constant.validateFields(timeET1.getText().toString())) {
            timeET1.setError("Please Enter Start Time.");
            status = false;
        }
        if (!Constant.validateFields(timeET2.getText().toString())) {
            timeET2.setError("Please Enter End Time.");
            status = false;
        }
        if (!Constant.validateFields(communicationET.getText().toString())) {
            communicationET.setError("Please Enter Data in Communication Field.");
            status = false;
        }
        if (!Constant.validateFields(foodET.getText().toString())) {
            foodET.setError("Please Enter Data in Food Field.");
            status = false;
        }
        if (!Constant.validateFields(urineET.getText().toString())) {
            urineET.setError("Please Enter Data in Urine Field.");
            status = false;
        }
        if (!Constant.validateFields(bowlesET.getText().toString())) {
            bowlesET.setError("Please Enter Data in Bowles Field.");
            status = false;
        }
        if (!Constant.validateFields(printET.getText().toString())) {
            printET.setError("Please Enter Data in Print Field.");
            status = false;
        }
        if (!Constant.validateFields(signatureET.getText().toString())) {
            signatureET.setError("Please Enter Data in Signature Field.");
            status = false;
        }


        return status;
    }

    private void updateLabel() {

        String myFormat = "dd-MM-yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        dateET.setText(sdf.format(myCalendar.getTime()));
    }

    @Override
    public void onTimeSet(TimePicker timePicker, int i, int i1) {

        switch (whichView) {

            case "time1":

                Log.d("time", "hour" + i + "minute" + i1);
                timeET1.setText(i + ":" + i1 + ":" + "00");
                break;
            case "time2":
                timeET2.setText(i + ":" + i1 + ":" + "00");
                break;
        }

    }

    private void disableorEnableViews(boolean state) {
        dateET.setEnabled(state);
        timeET1.setEnabled(state);
        timeET2.setEnabled(state);
        communicationET.setEnabled(state);
        foodET.setEnabled(state);
        urineET.setEnabled(state);
        bowlesET.setEnabled(state);
        printET.setEnabled(state);
        signatureET.setEnabled(state);
    }
    private void resetFields() {
        dateET.setText("");
        timeET1.setText("");
        timeET2.setText("");
        communicationET.setText("");
        foodET.setText("");
        urineET.setText("");
        bowlesET.setText("");
        printET.setText("");
        signatureET.setText("");
    }

}
