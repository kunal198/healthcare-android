package com.natalie.healthcare.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.natalie.healthcare.Activities.HomePage;
import com.natalie.healthcare.Adapters.MedicationDeatilsAdapter;
import com.natalie.healthcare.Async_Thread.Super_AsyncTask;
import com.natalie.healthcare.Async_Thread.Super_AsyncTask_Interface;
import com.natalie.healthcare.Bean.MedicationPlans;
import com.natalie.healthcare.R;
import com.natalie.healthcare.Utills.Constant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by brst-pc20 on 1/6/17.
 */

public class MedicationPlanDetails extends Fragment implements View.OnClickListener {
    String client_id = "", path = "", client_name = "";
    RecyclerView medicationPlanRV;
    TextView nodataFoundTV;
    MedicationDeatilsAdapter medicationDeatilsAdapter;
    ArrayList<MedicationPlans> medicationPlansesList;

    public MedicationPlanDetails() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Constant.hideKeyboard(getActivity());
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.medication_plan_details, container, false);
        initView(view);


        return view;
    }

    private void initView(View view) {
        Bundle bundle = getArguments();
        if (bundle != null) {

            client_id = bundle.getString("client_id");
            path = bundle.getString("path");
            client_name = bundle.getString("client_name");


        }
        medicationPlansesList = new ArrayList<>();
        ((HomePage) getActivity()).addIV.setOnClickListener(MedicationPlanDetails.this);
        medicationPlanRV = (RecyclerView) view.findViewById(R.id.medicationPlanRV);
        nodataFoundTV = (TextView) view.findViewById(R.id.nodataFoundTV);
        medicationDeatilsAdapter = new MedicationDeatilsAdapter(getActivity(), medicationPlansesList, MedicationPlanDetails.this, client_id, medicationPlanRV);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        medicationPlanRV.setLayoutManager(mLayoutManager);
        medicationPlanRV.setItemAnimator(new DefaultItemAnimator());
        medicationPlanRV.setAdapter(medicationDeatilsAdapter);

        if (path.equals("care_plan")) {
            getClientCarePlans();
        } else if (path.equals("medication_chart")) {
            getMedicalCarePlans();
        } else if (path.equals("client_support_plan")) {
            getClientSupportPlan();
        }

    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.addIV:
                if (path.equals("care_plan")) {
                    CarePlan carePlan = new CarePlan();
                    carePlan.setArguments(getArguments());
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragmentsContainer2, carePlan).addToBackStack(null)
                            .commit();
                } else if (path.equals("medication_chart")) {
                    MedicationChart medicationChart = new MedicationChart();

                    medicationChart.setArguments(getArguments());
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragmentsContainer2, medicationChart).addToBackStack(null)
                            .commit();
                }


                break;


        }

    }

    @Override
    public void onResume() {
        super.onResume();

        if (path.equals("care_plan")) {
            HomePage.fragActivitytitleTV.setText("Care Plan");
            ((HomePage) getActivity()).addIV.setVisibility(View.VISIBLE);
        } else if (path.equals("medication_chart")) {
            HomePage.fragActivitytitleTV.setText("Medication Plan Details");
            ((HomePage) getActivity()).addIV.setVisibility(View.VISIBLE);
        } else if (path.equals("client_support_plan")) {
            HomePage.fragActivitytitleTV.setText("Client Support Plan");
            ((HomePage) getActivity()).addIV.setVisibility(View.GONE);

        }
        ((HomePage) getActivity()).logoutTV.setVisibility(View.GONE);
        ((HomePage) getActivity()).backIV.setVisibility(View.GONE);
    }

    private void getClientCarePlans() {
        HashMap hashMap = new HashMap();
        hashMap.put("user_id", Constant.USER_ID);
        hashMap.put("client_id", client_id);
        Log.d("hashmapp", "" + hashMap);
        Constant.execute(new Super_AsyncTask(getActivity(), hashMap, Constant.CLIENT_CARE_PLANS, new Super_AsyncTask_Interface() {

            @Override
            public void onTaskCompleted(String output) {
                Log.d("client_care_opt", "" + output);
                try {
                    JSONObject jsonObject = new JSONObject(output);
                    String message = jsonObject.optString("message");
                    String status = jsonObject.optString("status");
                    if (message.equalsIgnoreCase("Invalid Data")) {

                    } else if (status.equalsIgnoreCase("success")) {
                        medicationPlanRV.setVisibility(View.VISIBLE);
                        nodataFoundTV.setVisibility(View.GONE);
                        JSONArray jsonArray = jsonObject.optJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            MedicationPlans medicationPlans = new MedicationPlans();
                            JSONObject jsonObject1 = jsonArray.optJSONObject(i);
                            medicationPlans.setId(jsonObject1.optString("id"));
                            medicationPlans.setUser_id(jsonObject1.optString("user_id"));
                            medicationPlans.setClient_id(jsonObject1.optString("client_id"));
                            medicationPlans.setDate(jsonObject1.optString("date"));
                            medicationPlans.setStart_time(jsonObject1.optString("start_time"));
                            medicationPlans.setEnd_time(jsonObject1.optString("end_time"));
                            medicationPlans.setCommunication(jsonObject1.optString("communication"));
                            medicationPlans.setFood(jsonObject1.optString("food"));
                            medicationPlans.setUrine(jsonObject1.optString("urine"));
                            medicationPlans.setBowles(jsonObject1.optString("bowles"));
                            medicationPlans.setPrint(jsonObject1.optString("print"));
                            medicationPlans.setSignature(jsonObject1.optString("signature"));
                            medicationPlans.setPath("care_plans");
                            medicationPlansesList.add(medicationPlans);
                        }
                        medicationDeatilsAdapter.notifyDataSetChanged();

                    } else if (status.equalsIgnoreCase("error")) {
                        medicationPlanRV.setVisibility(View.GONE);
                        nodataFoundTV.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

        }, true, false));
    }

    private void getClientSupportPlan() {
        HashMap hashMap = new HashMap();
        hashMap.put("user_id", Constant.USER_ID);
        hashMap.put("client_id", client_id);
        Log.d("hashmapp", "" + hashMap);
        Constant.execute(new Super_AsyncTask(getActivity(), hashMap, Constant.Client_support_plans, new Super_AsyncTask_Interface() {

            @Override
            public void onTaskCompleted(String output) {
                Log.d("client_support_plan", "" + output);


                try {
                    JSONObject jsonObject = new JSONObject(output);
                    String message = jsonObject.optString("message");
                    String status = jsonObject.optString("status");
                    if (status.equalsIgnoreCase("success")) {
                        medicationPlanRV.setVisibility(View.VISIBLE);
                        nodataFoundTV.setVisibility(View.GONE);
                        JSONArray jsonArray = jsonObject.optJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            MedicationPlans medicationPlans = new MedicationPlans();
                            JSONObject jsonObject1 = jsonArray.optJSONObject(i);
                            medicationPlans.setId(jsonObject1.optString("id"));
                            medicationPlans.setUser_id(jsonObject1.optString("user_id"));
                            medicationPlans.setClient_id(jsonObject1.optString("client_id"));

                            medicationPlans.setService_user_name(jsonObject1.optString("service_user_name"));
                            medicationPlans.setAssessmentDate(jsonObject1.optString("assessment_date"));

                            medicationPlans.setReassessmentDate(jsonObject1.optString("reassessment_date"));
                            medicationPlans.setLifeHistory(jsonObject1.optString("life_history"));
                            medicationPlans.setHowToSupport(jsonObject1.optString("how_to_support"));
                            medicationPlans.setMorningVisits(jsonObject1.optString("morning_visits"));
                            medicationPlans.setDinnerVisits(jsonObject1.optString("dinner_visits"));

                            medicationPlans.setTeaVisits(jsonObject1.optString("tea_visits"));
                            medicationPlans.setNightVisits(jsonObject1.optString("night_visits"));
                            medicationPlans.setMedicationSupport(jsonObject1.optString("medication_support"));
                            medicationPlans.setSupportAroundHome(jsonObject1.optString("support_around_home"));


                            medicationPlans.setSocialNeedsEmotionalSupportRequirement(jsonObject1.optString("social_needs_emotional_support_requirements"));
                            medicationPlans.setImportantServiceUser((jsonObject1.optString("importance_service_user")));
                            medicationPlans.setFoodAlergies((jsonObject1.optString("food_allergies")));
                            medicationPlans.setOtherAllergies((jsonObject1.optString("other_allergies")));
                            medicationPlans.setDietaryRequirements((jsonObject1.optString("dietary_requirements")));


                            medicationPlans.setFavNonFavFoods((jsonObject1.optString("favourite_non_favourite_foods")));

                            medicationPlans.setSpecialRequirements((jsonObject1.optString("special_requirements")));
                            medicationPlans.setSupportHours((jsonObject1.optString("support_hours")));
                            medicationPlans.setOtherHours((jsonObject1.optString("other_hours")));


                            medicationPlans.setHealthHistory((jsonObject1.optString("health_history")));
                            medicationPlans.setDnar((jsonObject1.optString("dnar")));
                            medicationPlans.setSightHearing((jsonObject1.optString("sight_hearing")));


                            medicationPlans.setMentalHealth((jsonObject1.optString("mental_health")));
                            medicationPlans.setVariousProblems((jsonObject1.optString("various_problems")));
                            medicationPlans.setOtherProblems((jsonObject1.optString("other_problems")));
                            medicationPlans.setProneProblems((jsonObject1.optString("prone_problems")));
                            medicationPlans.setBlisterPacks((jsonObject1.optString("blister_packs")));


                            medicationPlans.setEyedrops((jsonObject1.optString("eye_drops")));
                            medicationPlans.setMedicationSafeNumber((jsonObject1.optString("medication_safe_number")));
                            medicationPlans.setMedicineBottles((jsonObject1.optString("medicine_bottles")));
                            medicationPlans.setOther((jsonObject1.optString("other")));
                            medicationPlans.setSupportLevel((jsonObject1.optString("support_level")));


                            medicationPlans.setPrescription((jsonObject1.optString("prescriptions")));
                            medicationPlans.setSideEffects((jsonObject1.optString("side_effects")));
                            medicationPlans.setCarePackageByHospital((jsonObject1.optString("care_package_by_hospital")));
                            medicationPlans.setPersonalInfo((jsonObject1.optString("personal_info")));


                            medicationPlans.setGpDetails((jsonObject1.optString("gp_details")));
                            medicationPlans.setSocialWorkerName((jsonObject1.optString("social_worker_name")));
                            medicationPlans.setSocailWorkerTel((jsonObject1.optString("social_worker_tel")));
                            medicationPlans.setAdditionalInfo((jsonObject1.optString("additional_info")));


                            medicationPlans.setFrontDoorKeySafe((jsonObject1.optString("front_door_key_safe")));
                            medicationPlans.setFrontDoorKeySafeNumber((jsonObject1.optString("front_door_key_safe_number")));
                            medicationPlans.setNominatedKeyHolderName((jsonObject1.optString("nominated_key_holder_name")));


                            medicationPlans.setNominatedKeyHolderTel((jsonObject1.optString("nominated_key_holder_tel")));
                            medicationPlans.setLifeLine((jsonObject1.optString("life_line")));
                            medicationPlans.setStopClockLocation((jsonObject1.optString("stop_clock_location")));
                            medicationPlans.setGasPointLocation((jsonObject1.optString("gas_point_location")));
                            medicationPlans.setElectricityPointLocation((jsonObject1.optString("electricity_point_location")));

                            medicationPlans.setFuseBoxLocation((jsonObject1.optString("fusebox_location")));
                            medicationPlans.setEmergencyContacts((jsonObject1.optString("emergency_contacts")));
                            medicationPlans.setFallRisks((jsonObject1.optString("falls_risks")));
                            medicationPlans.setPressureScores((jsonObject1.optString("pressure_sores")));

                            medicationPlans.setDescisionSupportPlan((jsonObject1.optString("decisions_support_plan")));
                            medicationPlans.setContigencyPlan((jsonObject1.optString("contigency_plan")));
                            medicationPlans.setStaffSelection((jsonObject1.optString("staff_selection")));
                            medicationPlans.setSupportWorkerRequirements((jsonObject1.optString("support_worker_requirements")));
                            medicationPlans.setAdditionalNotes((jsonObject1.optString("additional_notes")));


                            medicationPlans.setSupportComment((jsonObject1.optString("support_comment")));
                            medicationPlans.setFrequencyDosage((jsonObject1.optString("frequency_dosage")));
                            medicationPlans.setMedicationFrequency((jsonObject1.optString("medication_frequency")));
                            medicationPlans.setInformedConsent((jsonObject1.optString("informed_consent")));


                            medicationPlans.setPulse8ltd((jsonObject1.optString("pulse8ltd")));
                            JSONObject jsonObject2 = new JSONObject(jsonObject1.optString("pulse8ltd"));
                            medicationPlans.setDate(jsonObject2.optString("date"));
                            medicationPlans.setPath("client_support_plan");
                            medicationPlansesList.add(medicationPlans);
                        }
                        medicationDeatilsAdapter.notifyDataSetChanged();

                    } else if (status.equalsIgnoreCase("error")) {
                        medicationPlanRV.setVisibility(View.GONE);
                        nodataFoundTV.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

        }, true, false));
    }

    private void getMedicalCarePlans() {
        HashMap hashMap = new HashMap();
        hashMap.put("user_id", Constant.USER_ID);
        hashMap.put("client_id", client_id);
        Log.d("hashmapp", "" + hashMap);
        Constant.execute(new Super_AsyncTask(getActivity(), hashMap, Constant.MEDICATION_CARE_PLANS, new Super_AsyncTask_Interface() {

            @Override
            public void onTaskCompleted(String output) {
                Log.d("medical_care_opt", "" + output);
                try {
                    JSONObject jsonObject = new JSONObject(output);
                    String message = jsonObject.optString("message");
                    String status = jsonObject.optString("status");
                    if (message.equalsIgnoreCase("Invalid Data")) {

                    } else if (status.equalsIgnoreCase("success")) {
                        medicationPlanRV.setVisibility(View.VISIBLE);
                        nodataFoundTV.setVisibility(View.GONE);
                        JSONArray jsonArray = jsonObject.optJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            MedicationPlans medicationPlans = new MedicationPlans();
                            JSONObject jsonObject1 = jsonArray.optJSONObject(i);
                            medicationPlans.setId(jsonObject1.optString("id"));
                            medicationPlans.setUser_id(jsonObject1.optString("user_id"));
                            medicationPlans.setClient_id(jsonObject1.optString("client_id"));
                            medicationPlans.setPatientName(jsonObject1.optString("patient_name"));
                            medicationPlans.setFacility(jsonObject1.optString("facility"));
                            medicationPlans.setDob(jsonObject1.optString("DOB"));
                            medicationPlans.setSex(jsonObject1.optString("sex"));
                            Log.d("getSex", "==" + jsonObject1.optString("sex"));
                            medicationPlans.setMedicationPharmacy(jsonObject1.optString("medication_pharmacy"));
                            medicationPlans.setMonth(jsonObject1.optString("month"));
                            medicationPlans.setYear(jsonObject1.optString("year"));
                            medicationPlans.setMedicationTimings(jsonObject1.optString("medication_timings"));
                            medicationPlans.setDiagnoseComment(jsonObject1.optString("diagnose_comment"));
                            medicationPlans.setDoctorName(jsonObject1.optString("doctor_name"));
                            medicationPlans.setDoctorSignature(jsonObject1.optString("doctor_signature"));
                            medicationPlans.setAllery(jsonObject1.optString("allergies"));
                            medicationPlans.setManagerNmae(jsonObject1.optString("manager_name"));
                            medicationPlans.setManagerSignature(jsonObject1.optString("manager_signature"));
                            medicationPlans.setDate(jsonObject1.optString("medication_date"));
                            medicationPlans.setPath("medical_plans");
                            medicationPlansesList.add(medicationPlans);
                        }
                        medicationDeatilsAdapter.notifyDataSetChanged();

                    } else if (status.equalsIgnoreCase("error")) {
                        medicationPlanRV.setVisibility(View.GONE);
                        nodataFoundTV.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

        }, true, false));
    }
}
