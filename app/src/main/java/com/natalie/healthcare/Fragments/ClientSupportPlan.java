package com.natalie.healthcare.Fragments;


import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.natalie.healthcare.Activities.HomePage;
import com.natalie.healthcare.Bean.MedicationPlans;
import com.natalie.healthcare.Bean.ObjectWrapperForBinder;
import com.natalie.healthcare.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by brst-pc20 on 3/28/17.
 */

public class ClientSupportPlan extends Fragment {
    int no = 1;

    @Nullable
    @Bind(R.id.serviceUserName)
    TextView serviceUserName;

    @Nullable
    @Bind(R.id.assessmentdate)
    TextView assessmentdate;

    @Nullable
    @Bind(R.id.reassementDate)
    TextView reassementDate;

    @Nullable
    @Bind(R.id.lifeHistory)
    TextView lifeHistory;
    @Nullable
    @Bind(R.id.howToSupport)
    TextView howToSupport;

    @Nullable
    @Bind(R.id.medicationSupport)
    TextView medicationSupport;
    @Nullable
    @Bind(R.id.supportAroundHome)
    TextView supportAroundHome;
    @Nullable
    @Bind(R.id.socailNeeds)
    TextView socailNeeds;
    @Nullable
    @Bind(R.id.importantService)
    TextView importantService;
    @Nullable
    @Bind(R.id.foodAllergy)
    TextView foodAllergy;

    @Nullable
    @Bind(R.id.otherAllergy)
    TextView otherAllergy;
    @Nullable
    @Bind(R.id.dietryRequrements)
    TextView dietryRequrements;
    @Nullable
    @Bind(R.id.favNonFav)
    TextView favNonFav;
    @Nullable
    @Bind(R.id.specialRequirement)
    TextView specialRequirement;
    @Nullable
    @Bind(R.id.monTime)
    TextView monTime;

    @Nullable
    @Bind(R.id.tueTime)
    TextView tueTime;
    @Nullable
    @Bind(R.id.wedTime)
    TextView wedTime;
    @Nullable
    @Bind(R.id.thutime)
    TextView thutime;
    @Nullable
    @Bind(R.id.friTime)
    TextView friTime;
    @Nullable
    @Bind(R.id.satTime)
    TextView satTime;
    @Nullable
    @Bind(R.id.sunTime)
    TextView sunTime;


    @Nullable
    @Bind(R.id.monLength)
    TextView monLength;
    @Nullable
    @Bind(R.id.tueLength)
    TextView tueLength;
    @Nullable
    @Bind(R.id.wedLength)
    TextView wedLength;
    @Nullable
    @Bind(R.id.thuLength)
    TextView thuLength;
    @Nullable
    @Bind(R.id.friLength)
    TextView friLength;
    @Nullable
    @Bind(R.id.satLength)
    TextView satLength;
    @Nullable
    @Bind(R.id.sunLength)
    TextView sunLength;

    @Nullable
    @Bind(R.id.others)
    TextView othersHours;
    @Nullable
    @Bind(R.id.healthAndMedicalHistory)
    TextView healthAndMedicalHistory;
    @Nullable
    @Bind(R.id.dnar)
    TextView dnar;
    @Nullable
    @Bind(R.id.otherProblems)
    TextView otherProblems;
    @Nullable
    @Bind(R.id.proneProblems)
    TextView proneProblems;
    @Nullable
    @Bind(R.id.blisterPacks)
    TextView blisterPacks;


    @Nullable
    @Bind(R.id.eyeDrops)
    TextView eyeDrops;
    @Nullable
    @Bind(R.id.medicaTionSafeNumber)
    TextView medicaTionSafeNumber;
    @Nullable
    @Bind(R.id.medicineBottles)
    TextView medicineBottles;
    @Nullable
    @Bind(R.id.other)
    TextView other;
    @Nullable
    @Bind(R.id.prescription)
    TextView prescription;
    @Nullable
    @Bind(R.id.sideEffect)
    TextView sideEffect;
    @Nullable
    @Bind(R.id.carePackage)
    TextView carePackage;


    @Nullable
    @Bind(R.id.piFirstName)
    TextView piFirstName;
    @Nullable
    @Bind(R.id.piSurName)
    TextView piSurName;
    @Nullable
    @Bind(R.id.piDob)
    TextView piDob;
    @Nullable
    @Bind(R.id.piNationality)
    TextView piNationality;
    @Nullable
    @Bind(R.id.piReligion)
    TextView piReligion;
    @Nullable
    @Bind(R.id.piAddress)
    TextView piAddress;
    @Nullable
    @Bind(R.id.gpName)
    TextView gpName;


    @Nullable
    @Bind(R.id.gpPracticeName)
    TextView gpPracticeName;
    @Nullable
    @Bind(R.id.gpPracticeTel)
    TextView gpPracticeTel;
    @Nullable
    @Bind(R.id.gpMobileTel)
    TextView gpMobileTel;
    @Nullable
    @Bind(R.id.gpAddress)
    TextView gpAddress;
    @Nullable
    @Bind(R.id.socialAddress1)
    TextView socialAddress1;
    @Nullable
    @Bind(R.id.socialAddress2)
    TextView socialAddress2;


    @Nullable
    @Bind(R.id.socialAddress3)
    TextView socialAddress3;
    @Nullable
    @Bind(R.id.socailName)
    TextView socailName;
    @Nullable
    @Bind(R.id.socialTel)
    TextView socialTel;
    @Nullable
    @Bind(R.id.aiCountry)
    TextView aiCountry;
    @Nullable
    @Bind(R.id.aiPostCode)
    TextView aiPostCode;
    @Nullable
    @Bind(R.id.aiHomeTel)
    TextView aiHomeTel;


    @Nullable
    @Bind(R.id.aiMobileTel)
    TextView aiMobileTel;
    @Nullable
    @Bind(R.id.firstDoorSafeNumber)
    TextView firstDoorSafeNumber;
    @Nullable
    @Bind(R.id.nominatedName)
    TextView nominatedName;
    @Nullable
    @Bind(R.id.nominatedContact)
    TextView nominatedContact;
    @Nullable
    @Bind(R.id.stopClock)
    TextView stopClock;
    @Nullable
    @Bind(R.id.gasPointLocation)
    TextView gasPointLocation;


    @Nullable
    @Bind(R.id.electricityPointLocation)
    TextView electricityPointLocation;
    @Nullable
    @Bind(R.id.fuseBox)
    TextView fuseBox;
    @Nullable
    @Bind(R.id.ecName)
    TextView ecName;
    @Nullable
    @Bind(R.id.ecRelationShip)
    TextView ecRelationShip;
    @Nullable
    @Bind(R.id.ecAddress)
    TextView ecAddress;
    @Nullable
    @Bind(R.id.ecSimilarAddress)
    TextView ecSimilarAddress;


    @Nullable
    @Bind(R.id.ecPostCode)
    TextView ecPostCode;
    @Nullable
    @Bind(R.id.ecContact)
    TextView ecContact;
    @Nullable
    @Bind(R.id.ecContact2)
    TextView ecContact2;
    @Nullable
    @Bind(R.id.dspHelpMedication)
    TextView dspHelpMedication;
    @Nullable
    @Bind(R.id.dspFinalMedication)
    TextView dspFinalMedication;
    @Nullable
    @Bind(R.id.dspHelpPersonalCare)
    TextView dspHelpPersonalCare;


    @Nullable
    @Bind(R.id.dspFinalPersonalCare)
    TextView dspFinalPersonalCare;
    @Nullable
    @Bind(R.id.dspHelpCare)
    TextView dspHelpCare;
    @Nullable
    @Bind(R.id.dspFinalCare)
    TextView dspFinalCare;
    @Nullable
    @Bind(R.id.dspHelpSupport)
    TextView dspHelpSupport;
    @Nullable
    @Bind(R.id.dspFinalSupport)
    TextView dspFinalSupport;
    @Nullable
    @Bind(R.id.dspHelpillnes)
    TextView dspHelpillnes;


    @Nullable
    @Bind(R.id.dspFinalillnes)
    TextView dspFinalillnes;
    @Nullable
    @Bind(R.id.dspHelpHospitality)
    TextView dspHelpHospitality;


    @Nullable
    @Bind(R.id.dspFinalHospitality)
    TextView dspFinalHospitality;
    @Nullable
    @Bind(R.id.dspHelpDate)
    TextView dspHelpDate;
    @Nullable
    @Bind(R.id.dspFinalDate)
    TextView dspFinalDate;
    @Nullable
    @Bind(R.id.dspHelpLifeThreat)
    TextView dspHelpLifeThreat;
    @Nullable
    @Bind(R.id.dspFinalLifeThreat)
    TextView dspFinalLifeThreat;
    @Nullable
    @Bind(R.id.contigencyPlan)
    TextView contigencyPlan;


    @Nullable
    @Bind(R.id.supportWorkerRequirement)
    TextView supportWorkerRequirement;
    @Nullable
    @Bind(R.id.skillNeeded)
    TextView skillNeeded;
    @Nullable
    @Bind(R.id.personalityNeeded)
    TextView personalityNeeded;
    @Nullable
    @Bind(R.id.anName)
    TextView anName;


    @Nullable
    @Bind(R.id.anRelationship)
    TextView anRelationship;
    @Nullable
    @Bind(R.id.p8Name)
    TextView p8Name;
    @Nullable
    @Bind(R.id.p8Wife)
    TextView p8Wife;
    @Nullable
    @Bind(R.id.p8NoName)
    TextView p8NoName;
    @Nullable
    @Bind(R.id.p8NoRelationship)
    TextView p8NoRelationship;


    @Nullable
    @Bind(R.id.informedConsentName)
    TextView informedConsentName;
    @Nullable
    @Bind(R.id.informedConsentSign)
    TextView informedConsentSign;
    @Nullable
    @Bind(R.id.informedConsentDate)
    TextView informedConsentDate;
    @Nullable
    @Bind(R.id.freqDosageTV)
    TextView freqDosageTV;

    @Nullable
    @Bind(R.id.medicationFrequencyTV)
    TextView medicationFrequencyTV;

    @Nullable
    @Bind(R.id.p8RepresentativeName)
    TextView p8RepresentativeName;


    @Nullable
    @Bind(R.id.p8RepresentativeSign)
    TextView p8RepresentativeSign;
    @Nullable
    @Bind(R.id.p8Date)
    TextView p8Date;
    @Nullable
    @Bind(R.id.p8Position)
    TextView p8Position;
    @Nullable
    @Bind(R.id.p8CommencementDate)
    TextView p8CommencementDate;
    @Nullable
    @Bind(R.id.p8ReviewDate)
    TextView p8ReviewDate;

    @Nullable
    @Bind(R.id.supportCommentTV)
    TextView supportCommentTV;


    @Bind(R.id.morningVisits)
    CheckBox morningVisits;
    @Bind(R.id.dinnerVisits)
    CheckBox dinnerVisits;
    @Bind(R.id.teaVisits)
    CheckBox teaVisits;
    @Bind(R.id.nightVisits)
    CheckBox nightVisits;

    @Bind(R.id.fullySIghted)
    CheckBox fullySIghted;
    @Bind(R.id.partialSight)
    CheckBox partialSight;
    @Bind(R.id.Blindness)
    CheckBox Blindness;
    @Bind(R.id.glau)
    CheckBox glau;

    @Bind(R.id.fullHear)
    CheckBox fullHear;
    @Bind(R.id.deafness)
    CheckBox deafness;


    @Bind(R.id.memmory)
    CheckBox memmory;

    @Bind(R.id.hunti)
    CheckBox hunti;
    @Bind(R.id.epi)
    CheckBox epi;
    @Bind(R.id.dizzi)
    CheckBox dizzi;


    @Bind(R.id.useOf)
    CheckBox useOf;
    @Bind(R.id.useOfHoist)
    CheckBox useOfHoist;


    @Bind(R.id.shortness)
    CheckBox shortness;


    @Bind(R.id.pressure)
    CheckBox pressure;
    @Bind(R.id.bruises)
    CheckBox bruises;
    @Bind(R.id.foot)
    CheckBox foot;
    @Bind(R.id.varicose)
    CheckBox varicose;


    @Bind(R.id.broken)
    CheckBox broken;
    @Bind(R.id.ulcer)
    CheckBox ulcer;
    @Bind(R.id.pain)
    CheckBox pain;
    @Bind(R.id.diabetes)
    CheckBox diabetes;


    @Bind(R.id.weight)
    CheckBox weight;
    @Bind(R.id.oxygen)
    CheckBox oxygen;
    @Bind(R.id.eyeDrop)
    CheckBox eyeDrop;
    @Bind(R.id.tinni)
    CheckBox tinni;
    @Bind(R.id.inhalers)
    CheckBox inhalers;


    @Bind(R.id.noneService)
    CheckBox noneService;
    @Bind(R.id.genSupport)
    CheckBox genSupport;
    @Bind(R.id.administrationSpecial)
    CheckBox administrationSpecial;
    @Bind(R.id.administrationNonSpecial)
    CheckBox administrationNonSpecial;
    @Bind(R.id.assistingMedi)
    CheckBox assistingMedi;

    @Bind(R.id.keySafeYes)
    CheckBox keySafeYes;
    @Bind(R.id.keySafeNo)
    CheckBox keySafeNo;
    @Bind(R.id.lifeLineYes)
    CheckBox lifeLineYes;
    @Bind(R.id.lifeLineNo)
    CheckBox lifeLineNo;

    @Bind(R.id.fallHigh)
    CheckBox fallHigh;
    @Bind(R.id.fallMed)
    CheckBox fallMed;
    @Bind(R.id.fallLow)
    CheckBox fallLow;


    @Bind(R.id.pressureHigh)
    CheckBox pressureHigh;
    @Bind(R.id.pressureMed)
    CheckBox pressureMed;
    @Bind(R.id.pressureLow)
    CheckBox pressureLow;

    @Bind(R.id.staffYes)
    CheckBox staffYes;
    @Bind(R.id.staffNo)
    CheckBox staffNo;
    @Bind(R.id.staffNoMale)
    CheckBox staffNoMale;

    @Bind(R.id.additionalYes)
    CheckBox additionalYes;
    @Bind(R.id.additionalNo)
    CheckBox additionalNo;

    @Bind(R.id.pulseYes)
    CheckBox pulseYes;
    @Bind(R.id.pulseNo)
    CheckBox pulseNo;


    private String plan_id = "", client_name = "";

    private String client_id;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.client_support_plan, container, false);


        return view;


    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        initView(view);
    }

    @Override
    public void onResume() {
        super.onResume();
        HomePage.fragActivitytitleTV.setText("Client Support Plan");
        ((HomePage) getActivity()).addIV.setVisibility(View.GONE);
        ((HomePage) getActivity()).backIV.setVisibility(View.VISIBLE);
    }

    private void initView(View view) {
        Bundle bundle = getArguments();
        if (bundle != null) {

            client_id = bundle.getString("client_id", "");
            client_name = bundle.getString("client_name", "");

        }

        IBinder bundle1 = getArguments().getBinder("client_support_plan");
        if (bundle1 != null) {

            ((HomePage) getActivity()).addIV.setVisibility(View.GONE);

            ((HomePage) getActivity()).logoutTV.setVisibility(View.GONE);
            MedicationPlans objReceived = ((ObjectWrapperForBinder) getArguments().getBinder("client_support_plan")).getData();
            Log.d("assementdate", "=" + objReceived.getAssessmentDate());

            //extView assessmentdate= (TextView) view.findViewById(R.id.assessmentdate);
            serviceUserName.setText(objReceived.getService_user_name());
            assessmentdate.setText(objReceived.getAssessmentDate());
            reassementDate.setText(objReceived.getReassessmentDate());
            lifeHistory.setText(objReceived.getLifeHistory());

            supportCommentTV.setText(objReceived.getSupportComment());

            howToSupport.setText(objReceived.getHowToSupport());
            if (objReceived.getMorningVisits().equalsIgnoreCase("yes")) {
                morningVisits.setChecked(true);
                morningVisits.setEnabled(false);
            }
            if (objReceived.getDinnerVisits().equalsIgnoreCase("yes")) {
                dinnerVisits.setChecked(true);
                dinnerVisits.setEnabled(false);
            }
            if (objReceived.getTeaVisits().equalsIgnoreCase("yes")) {
                teaVisits.setChecked(true);
                teaVisits.setEnabled(false);
            }
            if (objReceived.getNightVisits().equalsIgnoreCase("yes")) {
                nightVisits.setChecked(true);
                nightVisits.setEnabled(false);
            }
            medicationSupport.setText(objReceived.getMedicationSupport());
            supportAroundHome.setText(objReceived.getSupportAroundHome());
            socailNeeds.setText(objReceived.getSocialNeedsEmotionalSupportRequirement());
            importantService.setText(objReceived.getImportantServiceUser());
            foodAllergy.setText(objReceived.getFoodAlergies());
            otherAllergy.setText(objReceived.getOtherAllergies());
            dietryRequrements.setText(objReceived.getDietaryRequirements());
            favNonFav.setText(objReceived.getFavNonFavFoods());
            specialRequirement.setText(objReceived.getSpecialRequirements());
            JSONObject jsonObject1 = null;
            JSONObject jsonObject2 = null;
            try {
                JSONObject jsonObject = new JSONObject(objReceived.getSupportHours());
                jsonObject1 = jsonObject.optJSONObject("length_of_visit");
                jsonObject2 = jsonObject.optJSONObject("time");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Iterator iter = jsonObject1.keys();
            Iterator iter2 = jsonObject2.keys();
            while (iter.hasNext()) {
                String key = (String) iter.next();
                String data = jsonObject1.optString(key);
                if (key.equals("monday")) {
                    setDataOfLength(monLength, data);
                } else if (key.equals("tuesday")) {
                    setDataOfLength(tueLength, data);
                } else if (key.equals("wednesday")) {
                    setDataOfLength(wedLength, data);
                } else if (key.equals("thursday")) {
                    setDataOfLength(thuLength, data);
                } else if (key.equals("friday")) {
                    setDataOfLength(friLength, data);
                } else if (key.equals("saturday")) {
                    setDataOfLength(satLength, data);
                } else if (key.equals("sunday")) {
                    setDataOfLength(sunLength, data);
                }

            }
            while (iter2.hasNext()) {
                String key = (String) iter2.next();
                String data = jsonObject1.optString(key);
                if (key.equals("monday")) {
                    setDataOfLength(monTime, data);
                } else if (key.equals("tuesday")) {
                    setDataOfLength(tueTime, data);
                } else if (key.equals("wednesday")) {
                    setDataOfLength(wedTime, data);
                } else if (key.equals("thursday")) {
                    setDataOfLength(thutime, data);
                } else if (key.equals("friday")) {
                    setDataOfLength(friTime, data);
                } else if (key.equals("saturday")) {
                    setDataOfLength(satTime, data);
                } else if (key.equals("sunday")) {
                    setDataOfLength(sunTime, data);
                }

            }
            othersHours.setText(objReceived.getOtherHours());
            healthAndMedicalHistory.setText(objReceived.getHealthHistory());
            healthAndMedicalHistory.setText(objReceived.getHealthHistory());
            dnar.setText(objReceived.getDnar());

            try {
                JSONObject jsonObject = new JSONObject(objReceived.getSightHearing());

                if (jsonObject.optString("full_sighted").equals("1")) {
                    fullySIghted.setChecked(true);
                }

                if (jsonObject.optString("blindness").equals("1")) {
                    Blindness.setChecked(true);
                }
                if (jsonObject.optString("deafness").equals("1")) {
                    deafness.setChecked(true);
                }
                if (jsonObject.optString("partial_sighted").equals("1")) {
                    partialSight.setChecked(true);
                }
                if (jsonObject.optString("full_hearing").equals("1")) {
                    fullHear.setChecked(true);
                }
                if (jsonObject.optString("glaucoma").equals("1")) {
                    glau.setChecked(true);
                }
                JSONObject mental_object = new JSONObject(objReceived.getMentalHealth());
                if (mental_object.optString("memory_loss").equals("1")) {
                    memmory.setChecked(true);
                }
                if (mental_object.optString("huntington_disease").equals("1")) {
                    hunti.setChecked(true);
                }
                if (mental_object.optString("epilepsy").equals("1")) {
                    epi.setChecked(true);
                }
                if (mental_object.optString("dizziness").equals("1")) {
                    dizzi.setChecked(true);
                }
                if (mental_object.optString("mobility_aids_use").equals("1")) {
                    useOf.setChecked(true);
                }
                if (mental_object.optString("hoists_use").equals("1")) {
                    useOfHoist.setChecked(true);
                }

                JSONObject various_object = new JSONObject(objReceived.getVariousProblems());

                if (various_object.optString("breath_shortness").equals("1")) {
                    shortness.setChecked(true);
                }
                if (various_object.optString("pressure_sores").equals("1")) {
                    pressure.setChecked(true);
                }
                if (various_object.optString("bruises").equals("1")) {
                    bruises.setChecked(true);
                }
                if (various_object.optString("footcare").equals("1")) {
                    foot.setChecked(true);
                }
                if (various_object.optString("varicose_veins").equals("1")) {
                    varicose.setChecked(true);
                }
                if (various_object.optString("broken_skin").equals("1")) {
                    broken.setChecked(true);
                }
                if (various_object.optString("ulcer").equals("1")) {
                    ulcer.setChecked(true);
                }
                if (various_object.optString("pain").equals("1")) {
                    pain.setChecked(true);
                }
                if (various_object.optString("diabetes").equals("1")) {
                    diabetes.setChecked(true);
                }
                if (various_object.optString("weight").equals("1")) {
                    weight.setChecked(true);
                }
                if (various_object.optString("oxygen").equals("1")) {
                    oxygen.setChecked(true);
                }
                if (various_object.optString("eye_drops").equals("1")) {
                    eyeDrop.setChecked(true);
                }
                if (various_object.optString("tinnitus").equals("1")) {
                    tinni.setChecked(true);
                }
                if (various_object.optString("inhalers").equals("1")) {
                    inhalers.setChecked(true);
                }

                freqDosageTV.setText(objReceived.getFrequencyDosage());
                medicationFrequencyTV.setText(objReceived.getMedicationFrequency());
                otherProblems.setText(objReceived.getOtherProblems());
                proneProblems.setText(objReceived.getProneProblems());
                blisterPacks.setText(objReceived.getBlisterPacks());
                eyeDrops.setText(objReceived.getEyedrops());
                medicaTionSafeNumber.setText(objReceived.getMedicationSafeNumber());
                medicineBottles.setText(objReceived.getMedicineBottles());
                other.setText(objReceived.getOther());

            } catch (JSONException e) {
                e.printStackTrace();
            }


            JSONObject levelSupport = null;
            try {
                levelSupport = new JSONObject(objReceived.getSupportLevel());
                if (levelSupport.optString("none_service_user_administers_own_medicine").equals("1")) {
                    noneService.setChecked(true);
                }
                if (levelSupport.optString("general_support").equals("1")) {
                    genSupport.setChecked(true);
                }
                if (levelSupport.optString("administration_of_medicines_special").equals("1")) {
                    administrationSpecial.setChecked(true);
                }
                if (levelSupport.optString("administration_of_medicines_non_special").equals("1")) {
                    administrationNonSpecial.setChecked(true);
                }
                if (levelSupport.optString("assisting_with_medication").equals("1")) {
                    assistingMedi.setChecked(true);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            prescription.setText(objReceived.getPrescription());
            sideEffect.setText(objReceived.getSideEffects());
            carePackage.setText(objReceived.getCarePackageByHospital());
            try {
                JSONObject jsonObject = new JSONObject(objReceived.getPersonalInfo());

                piFirstName.setText(jsonObject.optString("first_name"));
                piSurName.setText(jsonObject.optString("sur_name"));
                piNationality.setText(jsonObject.optString("nationality"));
                piDob.setText(jsonObject.optString("dob"));
                piReligion.setText(jsonObject.optString("religion"));
                piAddress.setText(jsonObject.optString("address"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                JSONObject jsonObject = new JSONObject(objReceived.getGpDetails());

                gpName.setText(jsonObject.optString("gp_name"));
                gpPracticeName.setText(jsonObject.optString("practice_name"));
                gpPracticeTel.setText(jsonObject.optString("practice_tel"));
                gpMobileTel.setText(jsonObject.optString("mobile_tel"));
                gpAddress.setText(jsonObject.optString("address"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            socailName.setText(objReceived.getSocialWorkerName());
            socialTel.setText(objReceived.getSocailWorkerTel());

            try {
                JSONObject jsonObject = new JSONObject(objReceived.getAdditionalInfo());
                socialAddress1.setText(jsonObject.optString("address_line_1"));
                socialAddress2.setText(jsonObject.optString("address_line_2"));
                socialAddress3.setText(jsonObject.optString("address_line_3"));
                aiCountry.setText(jsonObject.optString("country"));
                aiPostCode.setText(jsonObject.optString("post_code"));
                aiHomeTel.setText(jsonObject.optString("home_tel"));
                aiMobileTel.setText(jsonObject.optString("mobile_tel"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            String key_safe = objReceived.getFrontDoorKeySafe();
            if (key_safe.equals("1"))
                keySafeYes.setChecked(true);
            else
                keySafeNo.setChecked(true);

            firstDoorSafeNumber.setText(objReceived.getFrontDoorKeySafeNumber());
            nominatedName.setText(objReceived.getNominatedKeyHolderName());
            nominatedContact.setText(objReceived.getNominatedKeyHolderTel());
            stopClock.setText(objReceived.getStopClockLocation());
            gasPointLocation.setText(objReceived.getGasPointLocation());
            electricityPointLocation.setText(objReceived.getElectricityPointLocation());
            fuseBox.setText(objReceived.getFuseBoxLocation());
            try {
                JSONObject jsonObject = new JSONObject(objReceived.getEmergencyContacts());

                ecName.setText(jsonObject.optString("name"));
                ecRelationShip.setText(jsonObject.optString("relation"));
                ecAddress.setText(jsonObject.optString("address"));
                ecSimilarAddress.setText(jsonObject.optString("similar_address"));
                ecPostCode.setText(jsonObject.optString("post_code"));
                ecContact.setText(jsonObject.optString("contact_1"));
                ecContact2.setText(jsonObject.optString("contact_2"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String falls_check_box = objReceived.getFallRisks();
            Log.d("falls_check_box", "=" + falls_check_box);

            if (falls_check_box.equalsIgnoreCase("high")) {
                fallHigh.setChecked(true);
            } else if (falls_check_box.equalsIgnoreCase("medium")) {
                fallMed.setChecked(true);
            } else if (falls_check_box.equalsIgnoreCase("low")) {
                fallLow.setChecked(true);
            }

            String falls_check_box1 = objReceived.getPressureScores();
            Log.d("falls_check_box1", "=" + falls_check_box);
            if (falls_check_box1.equalsIgnoreCase("high")) {
                pressureHigh.setChecked(true);
            } else if (falls_check_box1.equalsIgnoreCase("medium")) {
                pressureMed.setChecked(true);
            } else if (falls_check_box1.equalsIgnoreCase("low")) {
                pressureLow.setChecked(true);
            }

            try {
                JSONObject jsonObject = new JSONObject(objReceived.getDescisionSupportPlan());


                dspHelpMedication.setText(jsonObject.optJSONObject("medication").optString("helper"));
                dspHelpPersonalCare.setText(jsonObject.optJSONObject("personal_care").optString("helper"));
                dspHelpCare.setText(jsonObject.optJSONObject("care_support_provision").optString("helper"));
                dspHelpSupport.setText(jsonObject.optJSONObject("external_care").optString("helper"));
                dspHelpillnes.setText(jsonObject.optJSONObject("illness_provision").optString("helper"));
                dspHelpHospitality.setText(jsonObject.optJSONObject("medication").optString("helper"));
                dspHelpDate.setText(jsonObject.optJSONObject("day_decisions_treatment").optString("helper"));
                dspHelpLifeThreat.setText(jsonObject.optJSONObject("life_threatning_illness").optString("decsion_maker"));

                dspFinalMedication.setText(jsonObject.optJSONObject("medication").optString("decsion_maker"));
                dspFinalPersonalCare.setText(jsonObject.optJSONObject("personal_care").optString("decsion_maker"));
                dspFinalCare.setText(jsonObject.optJSONObject("care_support_provision").optString("decsion_maker"));
                dspFinalSupport.setText(jsonObject.optJSONObject("external_care").optString("decsion_maker"));
                dspFinalillnes.setText(jsonObject.optJSONObject("illness_provision").optString("decsion_maker"));
                dspFinalHospitality.setText(jsonObject.optJSONObject("medication").optString("decsion_maker"));
                dspFinalDate.setText(jsonObject.optJSONObject("day_decisions_treatment").optString("decsion_maker"));
                dspFinalLifeThreat.setText(jsonObject.optJSONObject("life_threatning_illness").optString("decsion_maker"));


            } catch (JSONException e) {
                e.printStackTrace();
            }

            contigencyPlan.setText(objReceived.getContigencyPlan());

          /*  JSONObject various_object = null;
            try {
                various_object = new JSONObject(objReceived.getStaffSelection());
            } catch (JSONException e) {
                e.printStackTrace();
            }*/

            Log.d("objReceived.get", "==" + objReceived.getStaffSelection());
            Log.d("objReceived.get1", "==" + objReceived.getFallRisks());
            Log.d("objReceived.get2", "==" + objReceived.getPressureScores());


            if (objReceived.getStaffSelection().contains("1")) {
                staffYes.setChecked(true);
            } else if (objReceived.getStaffSelection().contains("0")) {
                staffNo.setChecked(true);
            } else {
                staffNoMale.setChecked(true);

            }


            try {
                JSONObject jsonObject = new JSONObject(objReceived.getSupportWorkerRequirements());
                supportWorkerRequirement.setText(jsonObject.optString("support_needed"));
                skillNeeded.setText(jsonObject.optString("skill_needed"));
                personalityNeeded.setText(jsonObject.optString("shared_common_interesets"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (objReceived.getLifeLine().equals("yes")) {
                lifeLineYes.setChecked(true);
                lifeLineNo.setChecked(false);
            } else {

                lifeLineYes.setChecked(false);
                lifeLineNo.setChecked(true);
            }


            try {
                JSONObject jsonObject = new JSONObject(objReceived.getAdditionalNotes());
                if (jsonObject.optString("provide_signature").equals("yes")) {
                    additionalYes.setChecked(true);
                } else {
                    additionalNo.setChecked(true);
                }


                if (jsonObject.optString("provide_pulse8_details").equals("yes")) {
                    pulseYes.setChecked(true);
                } else {
                    pulseNo.setChecked(true);
                }

                anName.setText(jsonObject.optString("on_behalf_of_client_name"));
                anRelationship.setText(jsonObject.optString("on_behalf_of_client_name"));
                p8Name.setText(jsonObject.optString("name"));
                p8Wife.setText(jsonObject.optString("relation"));

                p8NoName.setText(jsonObject.optString("person_providing_requirement_name"));

                p8NoRelationship.setText(jsonObject.optString("person_providing_requirement_relation"));
                JSONArray jsonArray = new JSONArray(objReceived.getInformedConsent());
                JSONObject jsonObject3 = jsonArray.optJSONObject(0);


                informedConsentName.setText(jsonObject3.optString("client_name"));
                informedConsentDate.setText(jsonObject3.optString("date"));
                informedConsentSign.setText(jsonObject3.optString("signature"));


            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                JSONObject jsonObject = new JSONObject(objReceived.getPulse8ltd());
                p8RepresentativeName.setText(jsonObject.optString("rep_name"));
                p8RepresentativeSign.setText(jsonObject.optString("signature"));
                p8Date.setText(jsonObject.optString("date"));
                p8Position.setText(jsonObject.optString("position"));
                p8CommencementDate.setText(jsonObject.optString("package_start_date"));
                p8ReviewDate.setText(jsonObject.optString("review_date"));


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private void setDataOfLength(TextView textView, String data) {
        textView.setText(data);
    }


}

