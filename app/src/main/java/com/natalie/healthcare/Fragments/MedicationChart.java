package com.natalie.healthcare.Fragments;


import android.app.DatePickerDialog;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.natalie.healthcare.Activities.HomePage;
import com.natalie.healthcare.Adapters.MedicationChartAdpater;
import com.natalie.healthcare.Async_Thread.Super_AsyncTask;
import com.natalie.healthcare.Async_Thread.Super_AsyncTask_Interface;
import com.natalie.healthcare.Bean.MedicationPlans;
import com.natalie.healthcare.Bean.MedicationPlansBean;
import com.natalie.healthcare.Bean.ObjectWrapperForBinder;
import com.natalie.healthcare.Dialog.MonthYearPickerDialog;
import com.natalie.healthcare.R;
import com.natalie.healthcare.Utills.Constant;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by brst-pc20 on 3/28/17.
 */

public class MedicationChart extends Fragment implements View.OnClickListener {


    private RecyclerView chartRV;
    private TextView submitTV, sexTV;
    private EditText medicationPharmacyTV, facilityTV, patientNameTV, dobTV,  diagnosedTV,
            allergyTV, doctorNameTV, dateTV, signatureTV, managerTV, date2TV, signature2TV;
    public static TextView monthTV,yearTV;
    private CheckBox maleCB, femaleCB;
    ArrayList<MedicationPlansBean> medication_chat_list = new ArrayList<>();
    private MedicationChartAdpater medicationChartAdpater;
    private String client_id = "", gender = "";
    DatePickerDialog.OnDateSetListener date;
    private Calendar myCalendar;
    private int hour, minute;

    private String plan_id = "",client_name="";
    private String whichMode="save";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.medication_chart, null);
        initView(view);


        return view;


    }

    private void initView(View view) {
        Bundle bundle = getArguments();
        if (bundle != null) {

            client_id = bundle.getString("client_id");
            client_name = bundle.getString("client_name","");

        }
        medicationPharmacyTV = (EditText) view.findViewById(R.id.medicationPharmacyTV);
        sexTV = (TextView) view.findViewById(R.id.sexTV);
        facilityTV = (EditText) view.findViewById(R.id.facilityTV);
        patientNameTV = (EditText) view.findViewById(R.id.patientNameTV);
        dobTV = (EditText) view.findViewById(R.id.dobTV);
        monthTV = (TextView) view.findViewById(R.id.monthTV);
        yearTV = (TextView) view.findViewById(R.id.yearTV);
        diagnosedTV = (EditText) view.findViewById(R.id.diagnosedTV);
        allergyTV = (EditText) view.findViewById(R.id.allergyTV);
        doctorNameTV = (EditText) view.findViewById(R.id.doctorNameTV);
        dateTV = (EditText) view.findViewById(R.id.dateTV);
        signatureTV = (EditText) view.findViewById(R.id.signatureTV);
        managerTV = (EditText) view.findViewById(R.id.managerTV);
        date2TV = (EditText) view.findViewById(R.id.date2TV);
        signature2TV = (EditText) view.findViewById(R.id.signature2TV);
        maleCB = (CheckBox) view.findViewById(R.id.maleCB);
        femaleCB = (CheckBox) view.findViewById(R.id.femaleCB);
        yearTV.setOnClickListener(this);
        monthTV.setOnClickListener(this);
        maleCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Log.d("log_tag", "==" + b);
                if (b) {
                    femaleCB.setChecked(false);
                }
                else {
                    femaleCB.setChecked(true);
                }

            }
        });
        femaleCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Log.d("log_tag", "fe==" + b);
                if (b) {
                    maleCB.setChecked(false);
                }
                else {
                    maleCB.setChecked(true);
                }


            }
        });
        patientNameTV.setText(client_name);

        submitTV = (TextView) view.findViewById(R.id.submitTV);
        chartRV = (RecyclerView) view.findViewById(R.id.chartRV);

        dobTV.setOnClickListener(this);
        dateTV.setOnClickListener(this);
        date2TV.setOnClickListener(this);
        monthTV.setOnClickListener(this);
        ((HomePage) getActivity()).logoutTV.setOnClickListener(this);

        setDateDialog();
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MMM-dd");
        String formattedDate = df.format(c.getTime());
        Log.d("formattedDate","=="+formattedDate);
        dateTV.setText(formattedDate);
        date2TV.setText(formattedDate);
        submitTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gender = maleCB.isChecked() ? "Male" : femaleCB.isChecked() ? "Female" : "";
                if (validationOfData()) {
                    JSONObject jsonObject1 = new JSONObject();
                    ;

                    for (int i = 0; i < 31; i++) {
                        JSONObject jsonObject = new JSONObject();

                        try {
                            jsonObject.put("morning", medicationChartAdpater.morning_arr[i]);
                            jsonObject.put("lunch", medicationChartAdpater.lunch_arr[i]);
                            jsonObject.put("tea", medicationChartAdpater.tea_arr[i]);
                            jsonObject.put("night", medicationChartAdpater.night_arr[i]);
                            int no = i + 1;
                            jsonObject1.put("" + no, jsonObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                    Log.d("jsondata", "" + jsonObject1.toString());


                    HashMap<String, String> hashMap = new HashMap<String, String>();
                    hashMap.put("user_id", Constant.USER_ID);
                    hashMap.put("client_id", client_id);
                    hashMap.put("medication_pharmacy", medicationPharmacyTV.getText().toString());
                    hashMap.put("facility", facilityTV.getText().toString());
                    hashMap.put("patient_name", patientNameTV.getText().toString());
                    hashMap.put("month", monthTV.getText().toString());
                    hashMap.put("year", yearTV.getText().toString());
                    hashMap.put("allergies", allergyTV.getText().toString());
                    hashMap.put("doctor_name", doctorNameTV.getText().toString());
                    hashMap.put("medication_date", dateTV.getText().toString());
                    hashMap.put("doctor_signature", signatureTV.getText().toString());
                    hashMap.put("DOB", dobTV.getText().toString());
                    hashMap.put("sex", gender);
                    hashMap.put("diagnose_comment", diagnosedTV.getText().toString());
                    hashMap.put("patient_name", patientNameTV.getText().toString());
                    hashMap.put("manager_name", managerTV.getText().toString());
                    hashMap.put("manager_signature", signature2TV.getText().toString());
                    hashMap.put("medication_timings", jsonObject1.toString());

                    if (whichMode.equals("save")){
                        Log.d("mapp", "==" + hashMap);
                        Constant.execute(new Super_AsyncTask(getActivity(), hashMap, Constant.SAVE_CLIENT_MEDICATION_CARE_PLANS, new Super_AsyncTask_Interface() {
                            @Override
                            public void onTaskCompleted(String result) {

                                Log.d("save_plns", "==" + result);
                                if (!result.isEmpty()) {
                                    try {
                                        JSONObject jsonObject = new JSONObject(result);
                                        if (jsonObject.optString("status").equals("success"))
                                        {
                                            resetViews();
                                            medication_chat_list.clear();
                                            prepareDataList();
                                            medicationChartAdpater.notifyDataSetChanged();
                                        }
                                        Constant.showAlert(getActivity(), "Alert", jsonObject.optString("message"));


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }

                            }
                        }, true, false));
                    }
                    else if (whichMode.equals("EDIT")){
                        hashMap.put("plan_id",plan_id);
                        Constant.execute(new Super_AsyncTask(getActivity(), hashMap, Constant.UPDATE_MEDICATION_CARE_PLANS, new Super_AsyncTask_Interface() {
                            @Override
                            public void onTaskCompleted(String result) {

                                Log.d("edit_plns", "==" + result);
                                if (!result.isEmpty()) {
                                    try {
                                        JSONObject jsonObject = new JSONObject(result);
                                        if (jsonObject.optString("status").equals("success"))
                                        {
                                           submitTV.setVisibility(View.GONE);
                                            ((HomePage)getActivity()).logoutTV.setVisibility(View.VISIBLE);
                                        }
                                        Constant.showAlert(getActivity(), "Alert", jsonObject.optString("message"));


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }

                            }
                        }, true, false));
                    }

                }
            }
        });
        prepareDataList();
        IBinder bundle1 = getArguments().getBinder("medical_plans");
        if (bundle1 != null) {
                whichMode = "EDIT";
            ((HomePage) getActivity()).addIV.setVisibility(View.GONE);
            ((HomePage) getActivity()).logoutTV.setText("Edit");
            ((HomePage) getActivity()).logoutTV.setVisibility(View.VISIBLE);
            ((HomePage) getActivity()).logoutTV.setOnClickListener(MedicationChart.this);
            disableorEnableViews(false);
            submitTV.setVisibility(View.GONE);
            client_id = bundle.getString("client_id");
            MedicationPlans objReceived = ((ObjectWrapperForBinder) getArguments().getBinder("medical_plans")).getData();
            medicationPharmacyTV.setText(objReceived.getMedicationPharmacy());
            facilityTV.setText(objReceived.getFacility());
            patientNameTV.setText(objReceived.getPatientName());
            dobTV.setText(objReceived.getDob());
            monthTV.setText(objReceived.getMonth());
            yearTV.setText(objReceived.getYear());
            diagnosedTV.setText(objReceived.getDiagnoseComment());
            allergyTV.setText(objReceived.getAllery());
            plan_id = objReceived.getId();
            signatureTV.setText(objReceived.getDoctorSignature());
            signature2TV.setText(objReceived.getManagerSignature());
            Log.d("getSex","=="+objReceived.getSex());

            String gender = objReceived.getSex().equalsIgnoreCase("Male") ? "Male" : "Female";
            if (gender.equalsIgnoreCase("Male")) {
                maleCB.setChecked(true);
            } else {
                femaleCB.setChecked(true);
            }
            client_id = objReceived.getClient_id();
            doctorNameTV.setText(objReceived.getDoctorName());
            managerTV.setText(objReceived.getManagerNmae());
            date2TV.setText(objReceived.getDate());
            dateTV.setText(objReceived.getDate());
            try {
                JSONObject jsonObject = new JSONObject(objReceived.getMedicationTimings());
                Log.d("jsonObject", "=" + jsonObject);
                medication_chat_list.clear();
                for (int i = 0; i < jsonObject.length(); i++) {
                    Log.d("getMedicationTimings", "=" + i);
                    int no = i + 1;
                    JSONObject jsonObject1 = jsonObject.optJSONObject(no + "");
                    MedicationPlansBean medicationPlansBean = new MedicationPlansBean();
                    medicationPlansBean.setSerialNo(no + "");
                    Log.d("jsonObject1", "==" + jsonObject1.optString("morning"));
                    Log.d("jsonObject1", "==" + jsonObject1.optString("lunch"));
                    Log.d("jsonObject1", "==" + jsonObject1.optString("tea"));
                    Log.d("jsonObject1", "==" + jsonObject1.optString("night"));
                    medicationPlansBean.setMorning(jsonObject1.optString("morning"));
                    medicationPlansBean.setLunch(jsonObject1.optString("lunch"));
                    medicationPlansBean.setTea(jsonObject1.optString("tea"));
                    medicationPlansBean.setNight(jsonObject1.optString("night"));

                    medication_chat_list.add(medicationPlansBean);
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.d("dataaaaa", "received object=" + objReceived);
        }
        medicationChartAdpater = new MedicationChartAdpater(getActivity(), getActivity(), chartRV, medication_chat_list, getWidthOfDevice());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        chartRV.setLayoutManager(mLayoutManager);
        chartRV.setItemAnimator(new DefaultItemAnimator());
        chartRV.setNestedScrollingEnabled(true);

        chartRV.setAdapter(medicationChartAdpater);
        if (whichMode.equalsIgnoreCase("edit"))
        {
            medicationChartAdpater.resetView(false);
            medicationChartAdpater.notifyDataSetChanged();
        }
    }

    private void setDateDialog() {
        myCalendar = Calendar.getInstance();


        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        hour = myCalendar.get(Calendar.HOUR_OF_DAY);
        minute = myCalendar.get(Calendar.MINUTE);

        // Create a new instance of TimePickerDialog and return it

    }

    private void updateLabel() {

        String myFormat = "dd-MM-yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);


            dobTV.setText(sdf.format(myCalendar.getTime()));

    }

    private ArrayList<MedicationPlansBean> prepareDataList() {
        for (int i = 1; i <= 31; i++) {
            MedicationPlansBean medicationPlansBean = new MedicationPlansBean();
            medicationPlansBean.setSerialNo(i + "");
            medicationPlansBean.setMorning("");
            medicationPlansBean.setLunch("");
            medicationPlansBean.setTea("");
            medicationPlansBean.setNight("");
            medication_chat_list.add(medicationPlansBean);
        }

        return medication_chat_list;
    }

    private int getWidthOfDevice() {
        DisplayMetrics displayMetrics = getActivity().getResources().getDisplayMetrics();
        return displayMetrics.widthPixels / 6;


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.dobTV:
                datePicker();
                break; case R.id.yearTV:
                MonthYearPickerDialog monthYearPickerDialog=new MonthYearPickerDialog();
                monthYearPickerDialog.show(getActivity().getFragmentManager(),"");
                break;
            case R.id.monthTV:
                MonthYearPickerDialog monthYearPickerDialog1=new MonthYearPickerDialog();
                monthYearPickerDialog1.show(getActivity().getFragmentManager(),"");
                break;
            case R.id.logoutTV:
                Log.d("logoutclicked","logout");
                submitTV.setVisibility(View.VISIBLE);
                ((HomePage)getActivity()).logoutTV.setVisibility(View.GONE);
                disableorEnableViews(true);
                medicationChartAdpater.resetView(true);
                medicationChartAdpater.notifyDataSetChanged();
                break;


        }


    }

    private void datePicker() {
        new DatePickerDialog(getActivity(), date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    private boolean validationOfData() {
        boolean status = true;
        if (!Constant.validateFields(medicationPharmacyTV.getText().toString())) {
            medicationPharmacyTV.setError("Please Enter Data.");
            status = false;
        }
        if (!Constant.validateFields(facilityTV.getText().toString())) {
            facilityTV.setError("Please Enter Data.");
            status = false;
        }
        if (!Constant.validateFields(patientNameTV.getText().toString())) {
            patientNameTV.setError("Please Enter Patient Name.");
            status = false;
        }
        if (!Constant.validateFields(dobTV.getText().toString())) {
            dobTV.setError("Please Enter Date.");
            status = false;
        }
        if (!Constant.validateFields(monthTV.getText().toString())) {
            monthTV.setError("Please Enter Month.");
            status = false;
        }
        if (!Constant.validateFields(yearTV.getText().toString())) {
            yearTV.setError("Please Enter year.");
            status = false;
        }
        if (!Constant.validateFields(gender)) {
            sexTV.setError("Please Enter sex.");
            status = false;
        }
        if (!Constant.validateFields(diagnosedTV.getText().toString())) {
            diagnosedTV.setError("Please Enter Data.");
            status = false;
        }
        if (!Constant.validateFields(patientNameTV.getText().toString())) {
            patientNameTV.setError("Please Enter Patient name.");
            status = false;
        }
        if (!Constant.validateFields(allergyTV.getText().toString())) {
            allergyTV.setError("Please Enter Data.");
            status = false;
        }
        if (!Constant.validateFields(dateTV.getText().toString())) {
            dateTV.setError("Please Enter Date.");
            status = false;
        }
        if (!Constant.validateFields(signatureTV.getText().toString())) {
            signatureTV.setError("Please Enter signature.");
            status = false;
        }
        if (!Constant.validateFields(managerTV.getText().toString())) {
            managerTV.setError("Please Enter Manager name.");
            status = false;
        }
        if (!Constant.validateFields(date2TV.getText().toString())) {
            date2TV.setError("Please Enter date.");
            status = false;
        }
        if (!Constant.validateFields(signature2TV.getText().toString())) {
            signature2TV.setError("Please Enter signature.");
            status = false;
        }

        return status;
    }

    @Override
    public void onResume() {
        super.onResume();

        HomePage.fragActivitytitleTV.setText("Medication Plan");
        ((HomePage)getActivity()).addIV.setVisibility(View.GONE);
        ((HomePage)getActivity()).backIV.setVisibility(View.VISIBLE);
    }

    private void showError(EditText mEditText) {
        Animation shake = AnimationUtils.loadAnimation(getActivity(), R.anim.shake);
        mEditText.startAnimation(shake);
    }

    private void disableorEnableViews(boolean state) {
        medicationPharmacyTV.setEnabled(state);
        facilityTV.setEnabled(state);
        patientNameTV.setEnabled(state);
        dobTV.setEnabled(state);
        maleCB.setEnabled(state);
        femaleCB.setEnabled(state);
        monthTV.setEnabled(state);
        yearTV.setEnabled(state);
        diagnosedTV.setEnabled(state);
        allergyTV.setEnabled(state);
        doctorNameTV.setEnabled(state);
        signatureTV.setEnabled(state);
        managerTV.setEnabled(state);
        signature2TV.setEnabled(state);
    }
    private void resetViews() {
        medicationPharmacyTV.setText("");
        facilityTV.setText("");
        patientNameTV.setText("");
        dobTV.setText("");
        maleCB.setChecked(true);
        femaleCB.setChecked(false);
        monthTV.setText("");
        yearTV.setText("");
        diagnosedTV.setText("");
        allergyTV.setText("");
        doctorNameTV.setText("");
        signatureTV.setText("");
        managerTV.setText("");
        signature2TV.setText("");




    }



}
