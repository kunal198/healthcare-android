package com.natalie.healthcare.Async_Thread;

import org.apache.http.conn.ConnectTimeoutException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HttpsURLConnection;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 Created by Gagan on 8/18/15. */
public class WebServiceHandler
{

    public String performPostCall(String requestURL, HashMap<String, String> postDataParams)
    {

        URL url;
        String response = "";
        try
        {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setConnectTimeout(10000);
            conn.setReadTimeout(10000);
            conn.setChunkedStreamingMode(1024);

            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            writer.write(getPostDataString(postDataParams));

            writer.flush();
            writer.close();
            os.close();

            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK)
            {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

                while ((line = br.readLine()) != null)
                {

                    response += line;

                }

            }
            else
            {
                response = "ERROR";


            }
        }

        catch (SocketTimeoutException e)
        {
            e.printStackTrace();
            return "SLOW";
        }
        catch (ConnectTimeoutException e)
        {
            e.printStackTrace();
            return "SLOW";
        }
        catch (NullPointerException e)
        {
            e.printStackTrace();
            return "ERROR";
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return "SLOW";
        }

        return response;
    }

    private String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean       first  = true;
        for (Map.Entry<String, String> entry : params.entrySet())
        {
            if (first)
            {
                first = false;

            }
            else
            {
                result.append("&");

            }

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }

    public String performGetCall(String url) throws Exception
    {
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");
        con.setConnectTimeout(10000);
        con.setReadTimeout(10000);
        con.setChunkedStreamingMode(1024);
        //add request header

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'GET' request to URL : " + url);
        System.out.println("Response Code : " + responseCode);

        BufferedReader in       = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null)
        {
            response.append(inputLine);
        }
        in.close();




        return response.toString();

    }
    String res = "";

    public String getPostDataResponse(String url, HashMap<String, String> params) throws UnsupportedEncodingException
    {

        OkHttpClient client  =new OkHttpClient.Builder()
                .connectTimeout(7, TimeUnit.SECONDS)
                .writeTimeout(7, TimeUnit.SECONDS)
                .readTimeout(7, TimeUnit.SECONDS)
                .build();


        MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
        RequestBody body = RequestBody.create(mediaType, getPostDataString(params));
        Request request = new Request.Builder().url(url).post(body).addHeader("cache-control", "no-cache").addHeader("content-type", "application/x-www-form-urlencoded").build();

        try
        {
            Response response = client.newCall(request).execute();

            return res = response.body().string();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return res;

    }

    public String getGetDataResponse(String URL) throws UnsupportedEncodingException
    {
        OkHttpClient client =new OkHttpClient.Builder()
                .connectTimeout(7, TimeUnit.SECONDS)
                .writeTimeout(7, TimeUnit.SECONDS)
                .readTimeout(7, TimeUnit.SECONDS)
                .build();



        Request request = new Request.Builder().url(URL).get().addHeader("cache-control", "no-cache").build();


//        Response response = client.newCall(request).execute();}


        try
        {
            Response response = client.newCall(request).execute();

            return res = response.body().string();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return res;
    }
}