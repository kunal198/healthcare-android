package com.natalie.healthcare.Async_Thread;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;


import com.natalie.healthcare.R;
import com.natalie.healthcare.Utills.Constant;

import java.util.HashMap;


/**
 Created by sharan on 10/9/15. */
public class Super_AsyncTask extends AsyncTask<Void, Void, String>
{

    String URL;
    HashMap<String, String> inputData = null;
    Context con;
    ProgressDialog dialog;
    Super_AsyncTask_Interface listener= null;
    boolean show_progressbar_or_not = false;
    boolean show_percentage = false;
    private TextView progress_text;
     Constant myConstants= new Constant(con);

    public Super_AsyncTask(Context con, HashMap<String, String> inputData, String URL, Super_AsyncTask_Interface listener, boolean show_progressbar_or_not, boolean show_percentage)
    {
        this.con = con;
        this.inputData = inputData;
        this.URL = URL;
        this.listener = listener;
        this.show_progressbar_or_not = show_progressbar_or_not;
        this.show_percentage=show_percentage;
        myConstants.hideKeyboard(con);
    }

    public Super_AsyncTask(Context con, String URL, Super_AsyncTask_Interface listener, boolean show_progressbar_or_not, boolean show_percentage)
    {
        this.con = con;
        this.URL = URL;
        this.listener = listener;
        this.show_progressbar_or_not = show_progressbar_or_not;
        this.show_percentage=show_percentage;
    }



    @Override
    protected void onPreExecute()
    {       Log.e("show_progressbar_or_not", "" + show_progressbar_or_not);

        super.onPreExecute();
        if (show_progressbar_or_not)
        {
            dialog = ProgressDialog.show(con, "", "");
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialog.setContentView(R.layout.progress_dialog);
            if (show_percentage)
            {
                //progress_text= (TextView) dialog.findViewById(R.id.progress_text);
                progress_text.setText("20%");
            }
            dialog.show();
        }
    }

    @Override
    protected String doInBackground(Void... params)
    {
        String response = "";
  Log.e("inputData", "" + inputData);
        try
        {

            if (inputData != null)
            {
                response = new WebServiceHandler().getPostDataResponse(URL, inputData);
            }
            else
            {
                response = new WebServiceHandler().getGetDataResponse(URL);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return response;
    }

    @Override
    protected void onPostExecute(String ResponseString)
    {
        super.onPostExecute(ResponseString);
        if (show_percentage)
        {
            progress_text.setText("100%");
        }
        if (show_progressbar_or_not)
        {
            if (dialog.isShowing())
                dialog.dismiss();
        }

        Log.e("Response for " + con.getClass().getName(), " " + ResponseString);

        if (!ResponseString.equals("SLOW") && !ResponseString.equals("ERROR"))
        {
            listener.onTaskCompleted(ResponseString);

        }
        else if (ResponseString.equals("SLOW"))
        {

        }
        else if (ResponseString.equals("ERROR"))
        {

        }

    }
}
